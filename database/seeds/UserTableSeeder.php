<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' =>  'Super Admin',
                'username' => 'SuperAdmin',
                'role' => 'SuperAdmin',
                'password' => bcrypt('secret123'),
                'remember_token' => 'null',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],[
                'id' => 2,
                'name' =>  'John Joshua Jamora',
                'username' => 'jjamora0021',
                'role' => '5_pr_canvass',
                'password' => bcrypt('thc051588'),
                'remember_token' => 'null',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
