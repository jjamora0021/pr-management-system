<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
            	'role_id' => '1_bm',
            	'role' =>  'BM'
            ],
            [
            	'role_id' => '2_receiving',
            	'role' =>  'Receiving'
            ],
            [
            	'role_id' => '3_admin',
            	'role' =>  'Admin'
            ],
            [
                'role_id' => '4_communications',
                'role' =>  'Communications'
            ],
            [
            	'role_id' => '5_pr_canvass',
            	'role' =>  'PR Canvass'
            ],
            [
            	'role_id' => '6_checking',
            	'role' =>  'Checking'
            ],
            [
            	'role_id' => '7_philgeps',
            	'role' =>  'Philgeps'
            ],
            [
            	'role_id' => '8_p0',
            	'role' =>  'Purchase Order'
            ],
            [
            	'role_id' => '9_obr',
            	'role' =>  'OBR'
            ],
            [
            	'role_id' => '10_data_encoder',
            	'role' =>  'Data Encoder'
            ],
        ]);
    }
}
