<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class RequestingDepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = Carbon::now();
        DB::table('requesting_department')->insert([
            [
            	'dept_id' => 'city_assessors_office_0',
            	'department' =>  'City Assessor\'s Office',
            	'created_at' => $now,
            	'updated_at' => $now
            ],
            [
            	'dept_id' => 'city_veterinary_office_1',
            	'department' =>  'City Veterinary Office',
            	'created_at' => $now,
            	'updated_at' => $now
            ],
            [
            	'dept_id' => 'city_treasury_2',
            	'department' =>  'City Treasury',
            	'created_at' => $now,
            	'updated_at' => $now
            ],
            [
            	'dept_id' => 'city_hrmo_3',
            	'department' =>  'City HRMO',
            	'created_at' => $now,
            	'updated_at' => $now
            ],
            [
            	'dept_id' => 'city_ptrb_4',
            	'department' =>  'City PTRB',
            	'created_at' => $now,
            	'updated_at' => $now
            ],
            [
            	'dept_id' => 'city_mayors_office_5',
            	'department' =>  'City Mayor\'s Office',
            	'created_at' => $now,
            	'updated_at' => $now
            ],
            [
            	'dept_id' => 'city_accounting_office_6',
            	'department' =>  'City Accounting Office',
            	'created_at' => $now,
            	'updated_at' => $now
            ],
            [
            	'dept_id' => 'city_veterinary_office_7',
            	'department' =>  'City Veterinary Office',
            	'created_at' => $now,
            	'updated_at' => $now
            ]
        ]);
    }
}
