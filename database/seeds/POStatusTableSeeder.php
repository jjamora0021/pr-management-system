<?php

use Illuminate\Database\Seeder;

class POStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('po_status')->insert([
            [
            	'po_status_id' => 'in_progress',
            	'po_status' =>  'In Progress'
            ],
            [
            	'role_id' => 'pending',
            	'role' =>  'Pending'
            ],
            [
            	'role_id' => 'approved',
            	'role' =>  'Approved'
            ]
        ]);
    }
}
