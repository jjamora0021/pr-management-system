<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->string('user_id');
            $table->string('name');
            $table->string('role');
            $table->string('pr_number')->nullable();
            $table->string('pr_action')->nullable();
            $table->string('pr_action_status')->nullable();
            $table->string('pr_action_description')->nullable();
            $table->string('pr_status')->nullable();
            $table->string('po_number')->nullable();
            $table->string('po_action')->nullable();
            $table->string('po_action_description')->nullable();
            $table->string('po_status')->nullable();
            $table->timestamps(); // updated_at will serve the timestamp for forwarded to another dept.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
