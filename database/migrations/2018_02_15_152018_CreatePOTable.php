<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_table', function (Blueprint $table) {
            $table->increments('po_id');
            $table->string('po_number')->nullable();
            $table->string('pr_number')->unique();
            $table->string('po_status');
            $table->string('po_amount')->nullable();
            $table->string('supplier')->nullable();
            $table->string('obr_number')->nullable();
            $table->string('voucher_number')->nullable();
            $table->timestamps(); // updated_at will serve the timestamp for forwarded to another dept.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('po_table');
    }
}
