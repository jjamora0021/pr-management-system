<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCPRTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpr_document', function (Blueprint $table) {
            $table->string('supplier_id');
            $table->string('cpr_id');
            $table->longText('cpr');
            $table->string('cpr_expiration');
            $table->timestamps(); // updated_at will serve the timestamp for forwarded to another dept.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cpr_document');
    }
}
