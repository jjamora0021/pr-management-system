<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePRTable extends Migration
{
    public function up()
    {
        Schema::create('pr_table', function (Blueprint $table) {
            $table->increments('pr_id');
            $table->string('pr_number')->unique();
            $table->string('po_number')->nullable();
            $table->string('pr_amount');
            $table->string('requesting_dept');
            $table->string('item_description');
            $table->string('date_received');
            $table->string('received_by'); // Received by Dept
            $table->string('forwarded_to'); // Forwarded to Dept
            $table->string('created_at');
            $table->string('updated_at');
            $table->longText('document_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pr_table');
    }
}
