<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permits_document', function (Blueprint $table) {
            $table->string('supplier_id');
            $table->string('permits_id');
            $table->longText('permits');
            $table->string('permits_expiration');
            $table->timestamps(); // updated_at will serve the timestamp for forwarded to another dept.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permits_document');
    }
}
