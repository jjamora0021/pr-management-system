<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLTOTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lto_document', function (Blueprint $table) {
            $table->string('supplier_id');
            $table->string('lto_id');
            $table->longText('lto');
            $table->string('lto_expiration');
            $table->timestamps(); // updated_at will serve the timestamp for forwarded to another dept.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lto_document');
    }
}
