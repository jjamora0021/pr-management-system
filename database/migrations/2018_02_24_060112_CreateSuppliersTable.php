<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->string('supplier_id');
            $table->string('supplier');
            $table->longText('address');
            $table->string('mobile_number');
            $table->string('phone_number');
            $table->timestamps(); // updated_at will serve the timestamp for forwarded to another dept.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suppliers');
    }
}
