<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers_logs', function (Blueprint $table) {
            $table->string('user_id');
            $table->string('name');
            $table->string('role');
            $table->string('action');
            $table->string('action_status');
            $table->string('supplier_name');
            $table->string('supplier_document_type');
            $table->string('supplier_document_id');
            $table->string('supplier_document');
            $table->timestamps(); // updated_at will serve the timestamp for forwarded to another dept.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suppliers_logs');
    }
}
