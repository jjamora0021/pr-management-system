<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicenseToOperate extends Model
{
    protected $table = 'lto_document';
    protected $primaryKey = 'lto_id';

    public $incrementing = false;
}
