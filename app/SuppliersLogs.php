<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppliersLogs extends Model
{
    protected $table = 'suppliers_logs';
}
