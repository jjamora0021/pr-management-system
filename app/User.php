<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Auth;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'role', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function IsSA()
    {
        $user_role = Auth::user()->role;
        $role = 'SuperAdmin';
        if($user_role != $role)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function IsBM()
    {
        $user_role = Auth::user()->role;
        $role = '1_bm';
        if($user_role != $role)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function IsReceiving()
    {
        $user_role = Auth::user()->role;
        $role = '2_receiving';
        if($user_role != $role)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function IsAdmin()
    {
        $user_role = Auth::user()->role;
        $role = '3_admin';
        if($user_role != $role)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function IsCommunications()
    {
        $user_role = Auth::user()->role;
        $role = '4_communications';
        if($user_role != $role)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function IsCanvass()
    {
        $user_role = Auth::user()->role;
        $role = '5_pr_canvass';
        if($user_role != $role)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
