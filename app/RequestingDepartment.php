<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestingDepartment extends Model
{
	public $incrementing = false;
    protected $table = 'requesting_department';
    protected $primaryKey = 'dept_id';
}
