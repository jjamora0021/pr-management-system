<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permits extends Model
{
    protected $table = 'permits_document';
    protected $primaryKey = 'permits_id';

    public $incrementing = false;
}
