<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateOfProductRegistration extends Model
{
    protected $table = 'cpr_document';
    protected $primaryKey = 'cpr_document_id';

    public $incrementing = false;
}
