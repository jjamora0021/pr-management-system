<?php

namespace App\Http\Middleware;

use Closure;

class ForCommunicationsOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->IsCommunications())
        {
            return redirect('logout');
        }
        return $next($request);
    }
}
