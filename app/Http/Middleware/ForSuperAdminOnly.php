<?php

namespace App\Http\Middleware;

use Closure;

class ForSuperAdminOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->IsSA())
        {
            return redirect('logout');
        }
        return $next($request);
    }
}
