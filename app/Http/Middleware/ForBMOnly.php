<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ForBMOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->IsBM())
        {
            return redirect('logout');
        }

        return $next($request);
    }
}
