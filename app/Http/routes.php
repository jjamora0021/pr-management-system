<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('registration-success', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('register', function() {
	return view('auth.register');
});

Route::get('/auth/success', [
    'as'   => 'auth.success',
    'uses' => 'Auth\AuthController@Success'
]);



Route::get('login', function() {
	return view('auth.login');
});

Route::group(['middleware' => 'auth'], function() {
	/* Authenticate User and determine the page for it */
    Route::get('/authenticating-user', 'HomeController@index');

    /* Super Admin Section */
    Route::group(['middleware' => 'sa'], function() {
    	Route::get('registered-users', [
    		'as' => 'sa', 
    		'uses' => 'SAController@Users'
    	]);
    	Route::get('requesting-depts', [
    		'as' => 'depts', 
    		'uses' => 'SAController@Departments'
    	]);
    	Route::post('save-dept', [
    		'as' => 'save-dept', 
    		'uses' => 'SAController@SaveDepartment'
    	]);
    	Route::post('delete-dept', [
    		'as' => 'delete-dept', 
    		'uses' => 'SAController@DeleteDepartment'
    	]);
    });

    /* BM Section */
    Route::group(['middleware' => 'bm'], function() {
    	Route::get('bm-dashboard', [
    		'as' => 'bm', 
    		'uses' => 'BMController@Index'
    	]);
    });

    /* Receving Section */
    Route::group(['middleware' => 'receiving'], function() {
    	Route::get('receiving-dashboard', [
    		'as' => 'receiving', 
    		'uses' => 'ReceivingController@Index'
    	]);
    	Route::get('receiving-create-pr', [
    		'as' => 'create-pr', 
    		'uses' => 'ReceivingController@CreatePR'
    	]);
    	Route::post('receiving-save-pr', [
    		'as' => 'save-pr', 
    		'uses' => 'ReceivingController@SavePR'
    	]);
    	Route::post('receiving-delete-pr', [
    		'as' => 'delete-pr', 
    		'uses' => 'ReceivingController@DeletePR'
    	]);
    	Route::get('forward-to-admin', [
    		'as' => 'forward-to-admin',
    		'uses' => 'ReceivingController@ForwardToAdmin'
    	]);
    });

    /* Admin Section */
    Route::group(['middleware' => 'admin'], function() {
    	Route::get('admin-dashboard', [
    		'as' => 'admin', 
    		'uses' => 'AdminController@Index'
    	]);
    	Route::get('forward-to', [
    		'as' => 'forward-to',
    		'uses' => 'AdminController@ForwardTo'
    	]);
    });

    /* Communications Section */
    Route::group(['middleware' => 'communications'], function() {
    	Route::get('communications-dashboard', [
    		'as' => 'communications',
    		'uses' => 'CommunicationsController@Index'
    	]);
    });

    /* Canvass Section */
    Route::group(['middleware' => 'canvass'], function() {
    	Route::get('canvass-dashboard', [
    		'as' => 'canvass',
    		'uses' => 'CanvassController@Index'
    	]);
    	Route::get('suppliers', [
    		'as' => 'suppliers',
    		'uses' => 'SuppliersController@Suppliers'
    	]);
    	Route::post('save-suppliers', [
    		'as' => 'save-suppliers',
    		'uses' => 'SuppliersController@SaveSupplier'
    	]);
        Route::get('validate-supplier', [
            'as' => 'validate-supplier',
            'uses' => 'SuppliersController@ValidateSupplier'
        ]);
    });
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);

Route::auth();
