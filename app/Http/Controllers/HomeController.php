<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Auth::user()->role;
        switch ($role) {
            case 'SuperAdmin':
                return redirect()->route('sa'); 
                break;
            case '1_bm':
                return redirect()->route('bm');
                break;
            case '2_receiving':
                return redirect()->route('receiving');
                break;
            case '3_admin':
                return redirect()->route('admin');
                break;
            case '4_communications':
                return redirect()->route('communications');
                break;
            case '5_pr_canvass':
                return redirect()->route('canvass');
                break;
        }
    }
}
