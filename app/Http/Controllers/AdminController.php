<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PRModel;
use App\POModel;
use App\Logs;
use App\RequestingDepartment;

use Auth;
use Session;
use DB;
use Carbon\Carbon;



class AdminController extends Controller
{
    
	public function Index()
	{
		$query = DB::table('pr_table')
					->leftJoin('po_table', 'po_table.pr_number', '=', 'pr_table.pr_number')
					->leftJoin('po_status', 'po_status.po_status_id', '=', 'po_table.po_status')
					->leftJoin('requesting_department', 'requesting_department.dept_id', '=', 'pr_table.requesting_dept')
					->where('pr_table.forwarded_to','=','Admin')
					->get();
		$pr_list = array();
		foreach ($query as $key => $value) {
			$val = get_object_vars($value);
			
			$docu_arr = array();

			if($val['document_path'] === null)
			{
				$docu_arr = null;
			}
			else
			{
				$arr = json_decode($val['document_path']);
				foreach ($arr as $index => $el) {
					$ctr = count(explode('/', $el));
					
					$explode = explode('/', $el);
					$folder = $explode[$ctr - 2];
					
					$file = explode('/', $el);
					$file_name =  $file[$ctr - 1];

					$path = $folder . '/' .$file_name;
					
					$docu_arr[] = [
						'file_path' => 'upload-folder/'.$path,
						'file_name' => $file_name
					];
				}
			}
			

			$pr_list[] = [
				'pr_id' => $val['pr_id'],
				'pr_number' => $val['pr_number'],
				'pr_amount' => $val['pr_amount'],
				'po_number' => $val['po_number'],
				'po_amount' => $val['po_amount'],
				'po_status_id' => $val['po_status_id'],
				'po_status' => $val['po_status'],
				'dept_id' => $val['dept_id'],
				'department' => $val['department'],
				'item_description' => $val['item_description'],
				'received_by' => 'Admin',
				'forwarded_to' => 'Pending',
				'date_received' => $val['date_received'],
				'document' => $docu_arr
			];
		}
		return view('pages.admin.admin-dashboard', compact('pr_list'));
	}

	/* Forward To */
	public function ForwardTo(Request $request)
	{
		$user = Auth::user();
		$now = Carbon::now();
		$pr_id = $request['pr_id'];
		$pr_num = $request['pr_num'];
		$forwarded_to_id = $request['forwarded_to_id'];
		$forwarded_to_text = $request['forwarded_to_text'];
		$forward_pr_to = PRModel::find($pr_id);

		$forward_pr_to->received_by = 'Admin';
		$forward_pr_to->forwarded_to = $forwarded_to_text; 
		$forward_pr_to->updated_at = $now;

		$forward_pr_to->save();

		$query = DB::table('pr_table')
					->leftJoin('po_table', 'po_table.pr_number', '=', 'pr_table.pr_number')
					->leftJoin('po_status', 'po_status.po_status_id', '=', 'po_table.po_status')
					->leftJoin('requesting_department', 'requesting_department.dept_id', '=', 'pr_table.requesting_dept')
					->where('pr_table.forwarded_to','=','Admin')
					->get();
		// dd($query);
		$pr_list = array();
		foreach ($query as $key => $value) {
			$val = get_object_vars($value);
			
			$docu_arr = array();

			if($val['document_path'] === null)
			{
				$docu_arr = null;
			}
			else
			{
				$arr = json_decode($val['document_path']);
				foreach ($arr as $index => $el) {
					$ctr = count(explode('/', $el));
					
					$explode = explode('/', $el);
					$folder = $explode[$ctr - 2];
					
					$file = explode('/', $el);
					$file_name =  $file[$ctr - 1];

					$path = $folder . '/' .$file_name;
					
					$docu_arr[] = [
						'file_path' => 'upload-folder/'.$path,
						'file_name' => $file_name
					];
				}
			}


			$pr_list[] = [
				'pr_id' => $val['pr_id'],
				'pr_number' => $val['pr_number'],
				'pr_amount' => $val['pr_amount'],
				'po_number' => $val['po_number'],
				'po_amount' => $val['po_amount'],
				'po_status_id' => $val['po_status_id'],
				'po_status' => $val['po_status'],
				'dept_id' => $val['dept_id'],
				'department' => $val['department'],
				'item_description' => $val['item_description'],
				'received_by' => 'Admin',
				'forwarded_to' => 'Pending',
				'date_received' => $val['date_received'],
				'document' => $docu_arr
			];
		}
		
		if($forward_pr_to->save())
		{
			$logs = new Logs();

			$logs->user_id = $user->id;
			$logs->name = $user->name;
			$logs->role = $user->role;
			$logs->pr_number = $pr_num;
			$logs->pr_action = 'Update';
			$logs->pr_action_status = 'Success';
			$logs->pr_action_description = 'Successfully Forwarded to ' . $forwarded_to_text;
			$logs->pr_status = 'Forwarded To ' . $forwarded_to_text;
			$logs->po_number = null;
			$logs->po_action = null;
			$logs->po_action_description = null;
			$logs->po_status = null;
			$logs->created_at = $now;
			$logs->updated_at = $now;

			$logs->save();

			return response()->json($pr_list);
		}
		else
		{
			$logs = new Logs();

			$logs->user_id = $user->id;
			$logs->name = $user->name;
			$logs->role = $user->role;
			$logs->pr_number = $pr_num;
			$logs->pr_action = 'Update';
			$logs->pr_action_status = 'Failed';
			$logs->pr_action_description = 'Failed to Forward to ' . $forwarded_to_text;
			$logs->pr_status = null;
			$logs->po_number = null;
			$logs->po_action = null;
			$logs->po_action_description = null;
			$logs->po_status = null;
			$logs->created_at = $now;
			$logs->updated_at = $now;

			$logs->save();

			return 'Failed';
		}
	}
}
