<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\PRModel;
use App\POModel;
use App\Logs;
use App\RequestingDepartment;

use Auth;
use Session;
use DB;
use Carbon\Carbon;

class ReceivingController extends Controller
{
	/* Loads Receiving Dashboard */
	public function Index()
	{
		$query = DB::table('pr_table')
					->leftJoin('po_table', 'po_table.pr_number', '=', 'pr_table.pr_number')
					->leftJoin('po_status', 'po_status.po_status_id', '=', 'po_table.po_status')
					->leftJoin('requesting_department', 'requesting_department.dept_id', '=', 'pr_table.requesting_dept')
					->get();
		$pr_list = array();
		foreach ($query as $key => $value) {
			$val = get_object_vars($value);
			
			$docu_arr = array();

			if($val['document_path'] === null)
			{
				$docu_arr = null;
			}
			else
			{
				$arr = json_decode($val['document_path']);
				foreach ($arr as $index => $el) {
					$ctr = count(explode('/', $el));
					
					$explode = explode('/', $el);
					$folder = $explode[$ctr - 2];
					
					$file = explode('/', $el);
					$file_name =  $file[$ctr - 1];

					$path = $folder . '/' .$file_name;
					
					$docu_arr[] = [
						'file_path' => 'upload-folder/'.$path,
						'file_name' => $file_name
					];
				}
			}
			

			$pr_list[] = [
				'pr_id' => $val['pr_id'],
				'pr_number' => $val['pr_number'],
				'pr_amount' => $val['pr_amount'],
				'po_number' => $val['po_number'],
				'po_amount' => $val['po_amount'],
				'po_status_id' => $val['po_status_id'],
				'po_status' => $val['po_status'],
				'dept_id' => $val['dept_id'],
				'department' => $val['department'],
				'item_description' => $val['item_description'],
				'received_by' => $val['received_by'],
				'forwarded_to' => $val['forwarded_to'],
				'date_received' => $val['date_received'],
				'document' => $docu_arr
			];
		}
		return view('pages.receiving.receiving-dashboard', compact('pr_list'));
	}

	/* Loads Create PR Page */
	public function CreatePR()
	{
		$depts = DB::table('requesting_department')->get();
		return view('pages.receiving.receiving-create-pr', compact('depts'));	
	}

	/* Save Created PR */
	public function SavePR(Request $request)
	{
		$user = Auth::user();
		$pr_number = trim($request['pr_num']);
		/* Timestamp */
		$now = Carbon::now();
		/* Check if Purchase Request Already Exists */
		$pr_exists = DB::table('pr_table')->where('pr_number','=',$pr_number)->get();
		if(!empty($pr_exists))
		{
			$logs = new Logs();

			$logs->user_id = $user->id;
			$logs->name = $user->name;
			$logs->role = $user->role;
			$logs->pr_number = $pr_number;
			$logs->pr_action = 'Create';
			$logs->pr_action_status = 'Failed';
			$logs->pr_action_description = 'Failed to create a Purchase Reuqest. PR# already exists';
			$logs->pr_status = null;
			$logs->po_number = null;
			$logs->po_action = null;
			$logs->po_action_description = null;
			$logs->po_status = null;
			$logs->created_at = $now;
			$logs->updated_at = $now;

			$logs->save();

			return Redirect::action('ReceivingController@CreatePR')->with('danger', 'Purchase Reuqest Number Already Exists. Please try with an identifier.');
		}
		else
		{
			$pr_amount = trim($request['pr_amount']);
			$requesting_dept = trim($request['requesting_dept']);
			$item_desc = trim($request['item_desc']);
			$date_received = trim($request['date_received']);
			
			$files = $request->file('docu-upload');
			if($files[0] !== null)
			{
				$file_container = array();
				foreach ($files as $file) 
				{
					$filesize = $file->getClientSize();
					$file_n = $file->getClientOriginalName();
	                $maxSize = 12582912; //12MB
	                if($filesize > $maxSize) 
	                {
	                    return Redirect::action('ReceivingController@CreatePR')->with('danger', 'File of size ' .$file_n. ' is too big. Upload 2MB per file.');
	                } 
	                else 
	                {
	                	/* Creating File Name */
					    $filename = str_replace(' ', '_', strtolower($file->getClientOriginalName()));
				    	$folder_name = strtolower(str_replace(' ', '', $pr_number));
					    /* Creating Destination Folder */
					    $destination = 'upload-folder/'.$folder_name.'/';

					    /* Moving it to that folder */
					    $file->move($destination, $filename);

					    /* Dynamically Setting the root directory and the upload directory */
					    $baseURL = 'http://';
		                $serverPort = ':'.$_SERVER['SERVER_PORT'];
		                if (isset($_SERVER['HTTPS']) && strtoupper($_SERVER['HTTPS']) === 'ON'){
		                    $baseURL = 'https://';
		                }
		                $baseURL .= $_SERVER['SERVER_NAME'];
		                $protocol = (isset($_SERVER['HTTPS']) && strtoupper($_SERVER['HTTPS']) === 'ON') ? 'https://' : 'http://';
		                $currentFilePath = $_SERVER['DOCUMENT_ROOT'].dirname(dirname($_SERVER['PHP_SELF'])).'/public/'.$destination.$filename;

		                /* Storing all file paths to an array */
					    $file_container[] = $currentFilePath;
					    $docu_path = stripslashes(json_encode($file_container));
	                }
				}
			}
			else
			{
				$docu_path = null;
			}
			
			/* Create Entry for PR Table */
			$store_pr = new PRModel();

			$store_pr->pr_number = $pr_number;
			$store_pr->pr_amount = $pr_amount;
			$store_pr->po_number = null;
			$store_pr->requesting_dept = $requesting_dept;
			$store_pr->item_description = $item_desc;
			$store_pr->received_by = 'Receiving';
			$store_pr->forwarded_to = 'Pending';
			$store_pr->date_received = $date_received;
			$store_pr->document_path = $docu_path;
			$store_pr->created_at = $now;
			$store_pr->updated_at = $now;

			$store_pr->save();

			/* Create Entry for PO Table */
			$store_po = new POModel();

			$store_po->po_number = null;
			$store_po->pr_number = $pr_number;
			$store_po->po_status = 'in_progress';
			$store_po->po_amount = null;
			$store_po->supplier = null;
			$store_po->obr_number = null;
			$store_po->voucher_number = null;
			$store_po->created_at = $now;
			$store_po->updated_at = $now;

			if($store_pr->save() && $store_po->save())
			{
				$logs = new Logs();

				$logs->user_id = $user->id;
				$logs->name = $user->name;
				$logs->role = $user->role;
				$logs->pr_number = $pr_number;
				$logs->pr_action = 'Create';
				$logs->pr_action_status = 'Success';
				$logs->pr_action_description = 'Created a Purchase Reuqest with a PR# of: '.$pr_number.'.';
				$logs->pr_status = 'Received';
				$logs->po_number = null;
				$logs->po_action = null;
				$logs->po_action_description = null;
				$logs->po_status = 'in_progress';
				$logs->created_at = $now;
				$logs->updated_at = $now;

				$logs->save();
				return Redirect::action('ReceivingController@CreatePR')->with('success', 'Purchase Request Successfully Created.');
			}
		}
	}

	/* Delete PR */
	public function DeletePR(Request $request)
	{
		$now = Carbon::now();
		$user = Auth::user();
		$pr_num = $request['pr_number'];
		$pr_id = $request['pr_id'];
		$delete_pr = DB::table('pr_table')->where('pr_id','=',$pr_id)->delete();
				
		if($delete_pr)
		{
			$logs = new Logs();

			$logs->user_id = $user->id;
			$logs->name = $user->name;
			$logs->role = $user->role;
			$logs->pr_number = $pr_num;
			$logs->pr_action = 'Delete';
			$logs->pr_action_status = 'Success';
			$logs->pr_action_description = 'Successfully deleted a Purchase Request.';
			$logs->pr_status = null;
			$logs->po_number = null;
			$logs->po_action = null;
			$logs->po_action_description = null;
			$logs->po_status = null;
			$logs->created_at = $now;
			$logs->updated_at = $now;

			$logs->save();

			$delete_po = DB::table('po_table')->where('po_id','=',$pr_id)->delete();
			if($delete_po)
			{
				$logs = new Logs();

				$logs->user_id = $user->id;
				$logs->name = $user->name;
				$logs->role = $user->role;
				$logs->pr_number = $pr_num;
				$logs->pr_action = 'Delete';
				$logs->pr_action_status = 'Success';
				$logs->pr_action_description = 'Successfully deleted a Purchase Order.';
				$logs->pr_status = null;
				$logs->po_number = null;
				$logs->po_action = null;
				$logs->po_action_description = null;
				$logs->po_status = null;
				$logs->created_at = $now;
				$logs->updated_at = $now;

				$logs->save();
			}
			else
			{
				$logs = new Logs();

				$logs->user_id = $user->id;
				$logs->name = $user->name;
				$logs->role = $user->role;
				$logs->pr_number = $pr_num;
				$logs->pr_action = 'Delete';
				$logs->pr_action_status = 'Failed';
				$logs->pr_action_description = 'Failed to delete a Purchase Order.';
				$logs->pr_status = null;
				$logs->po_number = null;
				$logs->po_action = null;
				$logs->po_action_description = null;
				$logs->po_status = null;
				$logs->created_at = $now;
				$logs->updated_at = $now;

				$logs->save();
			}
			return Redirect::action('ReceivingController@Index')->with('success', 'Purchase Request Successfully Deleted.');
		}
		else
		{
			$logs = new Logs();

			$logs->user_id = $user->id;
			$logs->name = $user->name;
			$logs->role = $user->role;
			$logs->pr_number = $pr_num;
			$logs->pr_action = 'Delete';
			$logs->pr_action_status = 'Failed';
			$logs->pr_action_description = 'Failed to delete a Purchase Reuqest.';
			$logs->pr_status = null;
			$logs->po_number = null;
			$logs->po_action = null;
			$logs->po_action_description = null;
			$logs->po_status = null;
			$logs->created_at = $now;
			$logs->updated_at = $now;

			$logs->save();
			return Redirect::action('ReceivingController@Index')->with('danger', 'Purchase Request Failed to be Deleted.');
		}
	}

	/* Forward PR to Admin */
	public function ForwardToAdmin(Request $request)
	{
		$now = Carbon::now();
		$user = Auth::user();
		$pr_id = $request['pr_id'];
		$pr_num = $request['pr_num'];

		$update_pr = PRModel::find($pr_id);
		$update_pr->forwarded_to = 'Admin';
		$update_pr->updated_at = $now;

		$update_pr->save();

		if($update_pr->save())
		{
			$logs = new Logs();

			$logs->user_id = $user->id;
			$logs->name = $user->name;
			$logs->role = $user->role;
			$logs->pr_number = $pr_num;
			$logs->pr_action = 'Update';
			$logs->pr_action_status = 'Success';
			$logs->pr_action_description = 'Successfully Forwarded to Admin.';
			$logs->pr_status = 'Forwarded To Admin';
			$logs->po_number = null;
			$logs->po_action = null;
			$logs->po_action_description = null;
			$logs->po_status = null;
			$logs->created_at = $now;
			$logs->updated_at = $now;

			$logs->save();
		}
		else
		{
			$logs = new Logs();

			$logs->user_id = $user->id;
			$logs->name = $user->name;
			$logs->role = $user->role;
			$logs->pr_number = $pr_num;
			$logs->pr_action = 'Update';
			$logs->pr_action_status = 'Failed';
			$logs->pr_action_description = 'Failed to Forwarded to Admin.';
			$logs->pr_status = null;
			$logs->po_number = null;
			$logs->po_action = null;
			$logs->po_action_description = null;
			$logs->po_status = null;
			$logs->created_at = $now;
			$logs->updated_at = $now;

			$logs->save();

			return 'Failed';
		}

		$query = DB::table('pr_table')
				->leftJoin('po_table', 'po_table.pr_number', '=', 'pr_table.pr_number')
				->leftJoin('po_status', 'po_status.po_status_id', '=', 'po_table.po_status')
				->leftJoin('requesting_department', 'requesting_department.dept_id', '=', 'pr_table.requesting_dept')
				->get();
		if(!empty($query))
		{
			$pr_list = array();
			foreach ($query as $key => $value) {
				$val = get_object_vars($value);
				
				$docu_arr = array();

				if($val['document_path'] === null)
				{
					$docu_arr = null;
				}
				else
				{
					$arr = json_decode($val['document_path']);
					foreach ($arr as $index => $el) {
						$ctr = count(explode('/', $el));
						
						$explode = explode('/', $el);
						$folder = $explode[$ctr - 2];
						
						$file = explode('/', $el);
						$file_name =  $file[$ctr - 1];

						$path = $folder . '/' .$file_name;
						
						$docu_arr[] = [
							'file_path' => 'upload-folder/'.$path,
							'file_name' => $file_name
						];
					}
				}
				

				$pr_list[] = [
					'pr_id' => $val['pr_id'],
					'pr_number' => $val['pr_number'],
					'pr_amount' => $val['pr_amount'],
					'po_number' => $val['po_number'],
					'po_amount' => $val['po_amount'],
					'po_status_id' => $val['po_status_id'],
					'po_status' => $val['po_status'],
					'dept_id' => $val['dept_id'],
					'department' => $val['department'],
					'item_description' => $val['item_description'],
					'received_by' => $val['received_by'],
					'forwarded_to' => $val['forwarded_to'],
					'date_received' => $val['date_received'],
					'document' => $docu_arr
				];
			}
		}
		else
		{
			$pr_list = [];
		}
		
		if(empty($pr_list))
		{
			return 'Failed';
		}
		else
		{
			return response()->json($pr_list);
		}
	}
}
