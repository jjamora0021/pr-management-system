<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SuppliersLogs;
use App\Suppliers;
use App\Philgeps;
use App\LicenseToOperate;
use App\Permits;
use App\CertificateOfProductRegistration;

use Redirect;
use Auth;
use Session;
use DB;
use Carbon\Carbon;
use File;

class SuppliersController extends Controller
{

	public function Suppliers()
	{	
		$suppliers_query = DB::table('suppliers')->get();

		$suppliers = array();
		foreach ($suppliers_query as $key => $value) 
		{
			$val = get_object_vars($value);
			$mobile_num = ($val['mobile_number'] != 'Processing' ? json_decode($val['mobile_number']) : array((object)[1 => 'Processing']));
			$phone_num = ($val['phone_number'] != 'Processing' ? json_decode($val['phone_number']) : array((object)[1 => 'Processing']));
			
			// Mobile Number
			$mobile_arr = array();
			foreach ($mobile_num as $index => $mobile) {
				$mobile_arr[] = get_object_vars($mobile);
			}

			// Phone Number
			$phone_arr = array();
			foreach ($phone_num as $index => $phone) {
				$phone_arr[] = get_object_vars($phone);
			}
			
			// Philgeps
			$philgeps_query = DB::table('philgeps_document')->where('supplier_id','=',$val['supplier_id'])->get();
			$philgeps_docu = array();
			foreach ($philgeps_query as $indx => $philgeps) 
			{
				$phil = get_object_vars($philgeps);
				$philgeps_docu[] = [
					'philgeps_id' => $phil['philgeps_id'],
					'philgeps' => $phil['philgeps'],
					'philgeps_expiration' => $phil['philgeps_expiration']
				];
			}

			// LTO
			$lto_query = DB::table('lto_document')->where('supplier_id','=',$val['supplier_id'])->get();
			$lto_docu = array();
			foreach ($lto_query as $indx => $license) 
			{
				$lto = get_object_vars($license);
				$lto_docu[] = [
					'lto_id' => $lto['lto_id'],
					'lto' => $lto['lto'],
					'lto_expiration' => $lto['lto_expiration']
				];
			}

			// Permit
			$permit_query = DB::table('permits_document')->where('supplier_id','=',$val['supplier_id'])->get();
			$permit_docu = array();
			foreach ($permit_query as $indx => $permits) 
			{
				$permit = get_object_vars($permits);
				$permit_docu[] = [
					'permit_id' => $permit['permits_id'],
					'permit' => $permit['permits'],
					'permit_expiration' => $permit['permits_expiration']
				];
			} 

			// CPR
			$cpr_query = DB::table('cpr_document')->where('supplier_id','=',$val['supplier_id'])->get();
			$cpr_docu = array();
			foreach ($cpr_query as $indx => $certificate) 
			{
				$cpr = get_object_vars($certificate);
				$cpr_docu[] = [
					'cpr_id' => $cpr['cpr_id'],
					'cpr' => $cpr['cpr'],
					'cpr_expiration' => $cpr['cpr_expiration']
				];
			} 
			
			$suppliers[] = [
				'supplier_id' => $val['supplier_id'],
				'supplier' => $val['supplier'],
				'address' => $val['address'],
				'mobile_number' => $mobile_arr,
				'phone_number' => $phone_arr,
				'philgeps' => $philgeps_docu,
				'lto' => $lto_docu,
				'permit' => $permit_docu,
				'cpr' => $cpr_docu
			];
		}
		dump($suppliers);
		return view('pages.canvass.suppliers', compact('suppliers'));
	}

	public function ValidateSupplier(Request $request)
	{
		$query = DB::table('suppliers')->where('supplier','=',$request['name'])->get();
		if(!empty($query))
		{
			return 'taken';
		}
		else
		{
			return 'not yet';
		}
	}

	public function SaveSupplier(Request $request)
	{
		$now = Carbon::now();
		$suppliers = new Suppliers();
		
		// Supplier ID 
		$supplier_id = md5(rand());

		// Supplier Name
		$supplier_name = $request['suppliers-name'];
		// Supplier Address
		$supplier_address = $request['suppliers-address'];
		
		// Supplier Mobile Number
		if($request['suppliers-mobile-number'][0] == '') 
		{
			$supplier_mobile_number = 'Processing';
		} 
		else 
		{
			foreach ($request['suppliers-mobile-number'] as $key => $value) {
				$key++;
				$mobile_arr[] = [ $key => $value ];
			}
			$supplier_mobile_number = json_encode($mobile_arr);
		}
		
		// Supplier Phone Number
		if($request['suppliers-phone-number'][0] == '') 
		{
			$supplier_phone_number = 'Processing';
		} 
		else 
		{
			foreach ($request['suppliers-phone-number'] as $key => $value) {
				$key++;
				$phone_arr[] = [ $key => $value ];
			}
			$supplier_phone_number = json_encode($phone_arr);
		}
		$action = 'Create';

		// Philgeps
		$save_philgeps = $this->SavePhilgeps($request['philgeps-expiration'], $request['philgeps-upload'], $supplier_name, $supplier_id, $type = 'Philgeps', $action);
		
		// LTO
		$save_lto = $this->SaveLTO($request['lto-expiration'], $request['lto-upload'], $supplier_name, $supplier_id, $type = 'LTO', $action);

		// Permit
		$save_permit = $this->SavePermit($request['permit-expiration'], $request['permit-upload'], $supplier_name, $supplier_id, $type = 'Permit', $action);

		// CPR
		$save_cpr = $this->SaveCPR($request['cpr-expiration'], $request['cpr-upload'], $supplier_name, $supplier_id, $type = 'CPR', $action);
		
		// Supplier Info
		$suppliers->supplier_id = $supplier_id;
		$suppliers->supplier = $supplier_name;
		$suppliers->address = $supplier_address;
		$suppliers->mobile_number = $supplier_mobile_number;
		$suppliers->phone_number = $supplier_phone_number;
		$suppliers->created_at = $now;
		$suppliers->updated_at = $now;

		$suppliers->save();

		if($suppliers->save())
		{
			return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Successfully Added.');
		}
		else
		{
			return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Failed to be added. Please Try Again');
		}
	}

	public function SavePhilgeps($date, $docu, $supplier_name, $supplier_id, $type, $action)
	{
		$user = Auth::user();
		$now = Carbon::now();
		$result = array();
		if($date[0] == '') 
		{
			$id = md5(rand());
			$date = 'Processing';
			$docu = 'Processing';

			$philgeps = new Philgeps();

			$philgeps->supplier_id = $supplier_id;
			$philgeps->philgeps_id = $id;
			$philgeps->philgeps = $docu;
			$philgeps->philgeps_expiration = $date;
			$philgeps->created_at = $now;
			$philgeps->updated_at = $now;

			$philgeps->save();
			$action_status = ($philgeps->save() == true ? 'Success' : 'Failed');
			$result[] = [
					'user' => $user,
					'supplier_name' => $supplier_name,
					'docu_type' => $type,
					'document' => stripslashes(json_encode($docu)),
					'document_id' => $id,
					'action' => $action,
					'action_status' => $action_status
				];
		}
		else
		{
			$move_docu = $this->MoveFiles($supplier_name, $docu, $type);
			$result = array();
			foreach ($date as $key => $value) {
				$arr[$key] = [
					'supplier_id' => $supplier_id,
					'date' => $value,
					'docu' => $move_docu[$key]
				];
			}

			foreach ($arr as $key => $value) {
				$philgeps = new Philgeps();
				$id = md5(rand());

				$philgeps->supplier_id = $supplier_id;
				$philgeps->philgeps_id = $id;
				$philgeps->philgeps = stripslashes(json_encode($value['docu']));
				$philgeps->philgeps_expiration = $value['date'];
				$philgeps->created_at = $now;
				$philgeps->updated_at = $now;
				
				$philgeps->save();

				$action_status = ($philgeps->save() == true ? 'Success' : 'Failed');
				$result[] = [
					'user' => $user,
					'supplier_name' => $supplier_name,
					'docu_type' => $type,
					'document' => stripslashes(json_encode($value['docu'])),
					'document_id' => $id,
					'action' => $action,
					'action_status' => $action_status
				];
			}
		} 
		$log = $this->LogAction($result);
	}

	public function SaveLTO($date, $docu, $supplier_name, $supplier_id, $type, $action)
	{
		$user = Auth::user();
		$now = Carbon::now();
		$result = array();
		if($date[0] == '') 
		{
			$id = md5(rand());
			$date = 'Processing';
			$docu = 'Processing';

			$lto = new LicenseToOperate();

			$lto->supplier_id = $supplier_id;
			$lto->lto_id = $id;
			$lto->lto = $docu;
			$lto->lto_expiration = $date;
			$lto->created_at = $now;
			$lto->updated_at = $now;

			$lto->save();

			$action_status = ($lto->save() == true ? 'Success' : 'Failed');
			$result[] = [
				'user' => $user,
				'supplier_name' => $supplier_name,
				'docu_type' => $type,
				'document' => stripslashes(json_encode($docu)),
				'document_id' => $id,
				'action' => $action,
				'action_status' => $action_status
			];
		}
		else
		{
			$move_docu = $this->MoveFiles($supplier_name, $docu, $type);
			$result = array();
			foreach ($date as $key => $value) {
				$arr[$key] = [
					'supplier_id' => $supplier_id,
					'date' => $value,
					'docu' => $move_docu[$key]
				];
			}

			foreach ($arr as $key => $value) {
				$lto = new LicenseToOperate();
				$id = md5(rand());

				$lto->supplier_id = $supplier_id;
				$lto->lto_id = $id;
				$lto->lto = stripslashes(json_encode($value['docu']));
				$lto->lto_expiration = $value['date'];
				$lto->created_at = $now;
				$lto->updated_at = $now;
				
				$lto->save();

				$action_status = ($lto->save() == true ? 'Success' : 'Failed');
				$result[] = [
					'user' => $user,
					'supplier_name' => $supplier_name,
					'docu_type' => $type,
					'document' => stripslashes(json_encode($value['docu'])),
					'document_id' => $id,
					'action' => $action,
					'action_status' => $action_status
				];
			}
		} 
		$log = $this->LogAction($result);
	}

	public function SavePermit($date, $docu, $supplier_name, $supplier_id, $type, $action)
	{
		$user = Auth::user();
		$now = Carbon::now();
		$result = array();
		if($date[0] == '') 
		{
			$id = md5(rand());
			$date = 'Processing';
			$docu = 'Processing';

			$permit = new Permits();

			$permit->supplier_id = $supplier_id;
			$permit->permits_id = $id;
			$permit->permits = $docu;
			$permit->permits_expiration = $date;
			$permit->created_at = $now;
			$permit->updated_at = $now;

			$permit->save();

			$action_status = ($permit->save() == true ? 'Success' : 'Failed');
			$result[] = [
				'user' => $user,
				'supplier_name' => $supplier_name,
				'docu_type' => $type,
				'document' => stripslashes(json_encode($docu)),
				'document_id' => $id,
				'action' => $action,
				'action_status' => $action_status
			];
		}
		else
		{
			$move_docu = $this->MoveFiles($supplier_name, $docu, $type);
			$result = array();
			foreach ($date as $key => $value) {
				$arr[$key] = [
					'supplier_id' => $supplier_id,
					'date' => $value,
					'docu' => $move_docu[$key]
				];
			}

			foreach ($arr as $key => $value) {
				$permit = new Permits();
				$id = md5(rand());

				$permit->supplier_id = $supplier_id;
				$permit->permits_id = $id;
				$permit->permits = stripslashes(json_encode($value['docu']));
				$permit->permits_expiration = $value['date'];
				$permit->created_at = $now;
				$permit->updated_at = $now;
				
				$permit->save();

				$action_status = ($permit->save() == true ? 'Success' : 'Failed');
				$result[] = [
					'user' => $user,
					'supplier_name' => $supplier_name,
					'docu_type' => $type,
					'document' => stripslashes(json_encode($value['docu'])),
					'document_id' => $id,
					'action' => $action,
					'action_status' => $action_status
				];
			}
		} 
		$log = $this->LogAction($result);
	}

	public function SaveCPR($date, $docu, $supplier_name, $supplier_id, $type, $action)
	{
		$user = Auth::user();
		$now = Carbon::now();
		$result = array();
		if($date[0] == '') 
		{
			$id = md5(rand());
			$date = 'Processing';
			$docu = 'Processing';

			$cpr = new CertificateOfProductRegistration();

			$cpr->supplier_id = $supplier_id;
			$cpr->cpr_id = $id;
			$cpr->cpr = $docu;
			$cpr->cpr_expiration = $date;
			$cpr->created_at = $now;
			$cpr->updated_at = $now;

			$cpr->save();

			$action_status = ($cpr->save() == true ? 'Success' : 'Failed');
			$result[] = [
				'user' => $user,
				'supplier_name' => $supplier_name,
				'docu_type' => $type,
				'document' => stripslashes(json_encode($docu)),
				'document_id' => $id,
				'action' => $action,
				'action_status' => $action_status
			];
		}
		else
		{
			$move_docu = $this->MoveFiles($supplier_name, $docu, $type);
			$result = array();
			foreach ($date as $key => $value) {
				$arr[$key] = [
					'supplier_id' => $supplier_id,
					'date' => $value,
					'docu' => $move_docu[$key]
				];
			}

			foreach ($arr as $key => $value) {
				$cpr = new CertificateOfProductRegistration();
				$id = md5(rand());

				$cpr->cpr_id = $supplier_id;
				$cpr->cpr_id = $id;
				$cpr->cpr = stripslashes(json_encode($value['docu']));
				$cpr->cpr_expiration = $value['date'];
				$cpr->created_at = $now;
				$cpr->updated_at = $now;
				
				$cpr->save();

				$action_status = ($cpr->save() == true ? 'Success' : 'Failed');
				$result[] = [
					'user' => $user,
					'supplier_name' => $supplier_name,
					'docu_type' => $type,
					'document' => stripslashes(json_encode($value['docu'])),
					'document_id' => $id,
					'action' => $action,
					'action_status' => $action_status
				];
			}
		} 
		$log = $this->LogAction($result);
	}

	public function LogAction($result)
	{
		$now = Carbon::now();

		foreach ($result as $key => $value) {
			$suppliers_log = new SuppliersLogs();

			$suppliers_log->user_id = $value['user']->id;
			$suppliers_log->name = $value['user']->name;
			$suppliers_log->role = $value['user']->role;
			$suppliers_log->action = $value['action'];
			$suppliers_log->action_status = $value['action_status'];
			$suppliers_log->supplier_name = $value['supplier_name'];
			$suppliers_log->supplier_document = $value['document'];
			$suppliers_log->supplier_document_id = $value['document_id'];
			$suppliers_log->supplier_document_type = $value['docu_type'];
			$suppliers_log->created_at = $now;
			$suppliers_log->updated_at = $now;

			$suppliers_log->save();
		}
	}

	public function MoveFiles($supplier_name,$file_arr,$docu_type)
	{
		foreach ($file_arr as $key => $file) 
		{
			$filesize = $file->getClientSize();
			$file_n = $file->getClientOriginalName();
            $maxSize = 12582912; //12MB
            if($filesize > $maxSize) 
            {
                return Redirect::action('SuppliersController@Suppliers')->with('danger', 'File of size ' .$file_n. ' is too big. Upload 2MB per file.');
            } 
            else 
            {
            	/* Creating File Name */
			    $filename = str_replace(' ', '_', strtolower($file->getClientOriginalName()));
		    	$folder_name = $supplier_name;
			    /* Creating Destination Folder */
			    $destination = 'upload-folder/Suppliers/' . $folder_name .'/' . $docu_type . '/';

			    /* Moving it to that folder */
			    $file->move($destination, $filename);

			    /* Dynamically Setting the root directory and the upload directory */
			    $baseURL = 'http://';
                $serverPort = ':'.$_SERVER['SERVER_PORT'];
                if (isset($_SERVER['HTTPS']) && strtoupper($_SERVER['HTTPS']) === 'ON'){
                    $baseURL = 'https://';
                }
                $baseURL .= $_SERVER['SERVER_NAME'];
                $protocol = (isset($_SERVER['HTTPS']) && strtoupper($_SERVER['HTTPS']) === 'ON') ? 'https://' : 'http://';
                $currentFilePath = $_SERVER['DOCUMENT_ROOT'].dirname(dirname($_SERVER['PHP_SELF'])).'/public/'.$destination.$filename;

                /* Storing all file paths to an array */
			    $file_container[] = $currentFilePath;
            }
		}
		return $file_container;
	}

	public function DeleteSupplier(Request $request)
	{
		$id = $request['supplier_id'];
		$name = $request['supplier_name'];
		$delete_in_supplier = DB::table('suppliers')
					->where('supplier_id','=',$id)
					->delete();
		
		$now = Carbon::now();
		$user = Auth::user();
		$suppliers_logs = new SuppliersLogs();

		$docu = $suppliers_logs::select('supplier_document')->where('supplier_name', '=', $name)->orderBy('updated_at', 'desc')->first();

		if($delete_in_supplier)
		{
			$suppliers_logs->user_id = $user->id;
			$suppliers_logs->name = $user->name;
			$suppliers_logs->role = $user->role;
			$suppliers_logs->action = 'Delete';
			$suppliers_logs->action_status = 'Success';
			$suppliers_logs->supplier_name = $name;
			$suppliers_logs->supplier_document = $docu->supplier_document;
			$suppliers_logs->created_at = $now;
			$suppliers_logs->updated_at = $now;

			$suppliers_logs->save();

			return Redirect::action('SuppliersController@Suppliers')->with('success', $name . ' Successfully deleted.');
		}
		else
		{
			$suppliers_logs->user_id = $user->id;
			$suppliers_logs->name = $user->name;
			$suppliers_logs->role = $user->role;
			$suppliers_logs->action = 'Delete';
			$suppliers_logs->action_status = 'Failed';
			$suppliers_logs->supplier_name = $name;
			$suppliers_logs->supplier_document = $docu->supplier_document;
			$suppliers_logs->created_at = $now;
			$suppliers_logs->updated_at = $now;

			$suppliers_logs->save();

			return Redirect::action('SuppliersController@Suppliers')->with('danger', $name . ' has Failed to be deleted. Please try again.');
		}
	}

	public function UpdateSupplier(Request $request)
	{
		$now = Carbon::now();
		$user = Auth::user();

		$current_name = $request['current_supplier_name'];

		$supplier_id = $request['supplier_id'];
		$supplier_name = $request['supplier_name'];
		$supplier_address = $request['supplier_address'];
		$contact_info = [
			'mobile' => $request['supplier_mobile'],
			'phone' => $request['supplier_phone']
		];

		$supplier = DB::table('suppliers')
					->where('supplier_id','=',$supplier_id)
					->update([
						'supplier' => $supplier_name,
						'address' => $supplier_address,
						'contact_info' => json_encode($contact_info),
						'updated_at' => $now,
					]);

		$suppliers_logs = new SuppliersLogs();

		$docu = $suppliers_logs::select('supplier_document')->where('supplier_name', '=', $current_name)->orderBy('updated_at', 'desc')->first();

		if($supplier)
		{
			$suppliers_logs->user_id = $user->id;
			$suppliers_logs->name = $user->name;
			$suppliers_logs->role = $user->role;
			$suppliers_logs->action = 'Update';
			$suppliers_logs->action_status = 'Success';
			$suppliers_logs->supplier_name = $supplier_name;
			$suppliers_logs->supplier_document = $docu->supplier_document;
			$suppliers_logs->created_at = $now;
			$suppliers_logs->updated_at = $now;

			$suppliers_logs->save();

			return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' has been Successfully updated.');
		}
		else
		{
			$suppliers_logs->user_id = $user->id;
			$suppliers_logs->name = $user->name;
			$suppliers_logs->role = $user->role;
			$suppliers_logs->action = 'Update';
			$suppliers_logs->action_status = 'Failed';
			$suppliers_logs->supplier_name = $supplier_name;
			$suppliers_logs->supplier_document = $docu->supplier_document;
			$suppliers_logs->created_at = $now;
			$suppliers_logs->updated_at = $now;

			$suppliers_logs->save();

			return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Failed to updated.');
		}
	}

	public function GetSupplierInfo(Request $request)
	{
		$supplier_id = $request['id'];
		$info = DB::table('suppliers')->where('supplier_id','=',$supplier_id)->get();

		if(!empty($info[0]))
		{
			$contact_info = get_object_vars(json_decode(get_object_vars($info[0])['contact_info']));
			$arr = [
				'supplier' => get_object_vars($info[0])['supplier'],
				'address' => get_object_vars($info[0])['address'],
				'contact_info' => $contact_info
			];
			return response()->json($arr);
		}
		else
		{
			return 'Empty';
		}
	}

	public function AddDocument(Request $request)
	{
		$now = Carbon::now();
		$user = Auth::user();
		
		$type = $request['current_document_type'];
		$supplier_name = $request['supplier_name'];
		$supplier_id = $request['supplier_id'];
		$date = $request['document-expiration'];

		$docu_arr = $request->file('document-upload');
		// dd($type);
		switch ($type) {
			case 'philgeps':
				$philgeps_id = json_encode('philgeps_' . strtolower(str_replace(' ', '_', $supplier_name)));
				if($docu_arr[0] != null)
				{
					$docu_type = 'Philgeps';
					$philgeps_docu_path = $this->MoveFile($supplier_name,$docu_arr,$docu_type);
				} 
				else
				{
					$philgeps_docu_path = 'processing';
					$philgeps_docu_expiration = 'processing';
				}

				$update = DB::table('philgeps_document')
						->where('supplier_id','=',$supplier_id)
						->update([
							'philgeps' => json_encode($philgeps_docu_path),
							'philgeps_expiration' => $date,
							'updated_at' => $now,
						]);

				if($update)
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Create Philgeps Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($philgeps_docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));

					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Upload Success.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Create Philgeps Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($philgeps_docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));

					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Upload Failed.');
				}
			break;
			
			case 'license to operate':
				$lto_id = json_encode('lto_' . strtolower(str_replace(' ', '_', $supplier_name)));
				if($docu_arr[0] != null)
				{
					$docu_type = 'License To Operate';
					$lto_docu_path = $this->MoveFile($supplier_name,$docu_arr,$docu_type);
				} 
				else
				{
					$lto_docu_path = 'processing';
					$lto_docu_expiration = 'processing';
				}

				$update = DB::table('lto_document')
						->where('supplier_id','=',$supplier_id)
						->update([
							'lto' => json_encode($lto_docu_path),
							'lto_expiration' => $date,
							'updated_at' => $now,
						]);
				if($update === 1)
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update License To Operate Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($lto_docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Successfully Updated.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update License To Operate Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($lto_docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Update Failed.');
				}
			break;

			case 'permits':
				$ctr = DB::table('permits_document')->where('supplier_id','=',$supplier_id)->count();

				$ctr++;
				$permits_id = 'permits_' . strtolower(str_replace(' ', '_', $supplier_name)) . '_' . $ctr;
				
				if($docu_arr[0] != null)
				{
					$docu_type = 'Permits';
					$permits_docu_path = $this->MoveFile($supplier_name,$docu_arr,$docu_type);
				} 
				else
				{
					$permits_docu_path = 'processing';
					$permits_docu_expiration = 'processing';
				}

				$model = new Permits();

				$model->supplier_id = $supplier_id;
				$model->permits_id = $permits_id;
				$model->permits = stripslashes(json_encode($permits_docu_path[0]));
				$model->permits_expiration = $date;

				$model->save();

				if($model->save())
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Permits Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($permits_docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Successfully Updated.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Permits Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($permits_docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Update Failed.');
				}
			break;

			case 'cpr':
				$cpr_id = 'cpr_' . strtolower(str_replace(' ', '_', $supplier_name));
				if($docu_arr[0] != null)
				{
					$docu_type = 'CPR';
					$cpr_docu_path = $this->MoveFile($supplier_name,$docu_arr,$docu_type);
				} 
				else
				{
					$cpr_docu_path = 'processing';
					$cpr_docu_expiration = 'processing';
				}

				$model = new CertificateOfProductRegistration();

				$model->supplier_id = $supplier_id;
				$model->cpr_document_id = $cpr_id;
				$model->cpr_document = stripslashes(json_encode($cpr_docu_path[0]));
				$model->cpr_document_expiration = $date;

				$model->save();

				if($model->save())
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Certificate Of Product Registration Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($cpr_docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Successfully Updated.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Certificate Of Product Registration Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($cpr_docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Update Failed.');
				}
			break;
		}
	}

	public function DeleteSupplierDocument($type,$docu_id)
	{
		if($type == 'license to operate')
		{
			$type = 'license to operate';
		}
		else
		{
			$type = $type;
		}

		switch ($type) {
			case 'philgeps':
				$query = DB::table('philgeps_document')->select('philgeps')->where('philgeps_id','=',$docu_id)->get();
				if(!empty($query))
				{
					$old_file = json_decode(get_object_vars($query[0])['philgeps'])[0];
					File::delete($old_file);
				}
				else
				{
					return false;
				}
			break;
			
			case 'license to operate':
				$query = DB::table('lto_document')->select('lto')->where('lto_id','=',$docu_id)->get();
				if(!empty($query))
				{
					$old_file = json_decode(get_object_vars($query[0])['lto'])[0];
					File::delete($old_file);
				}
				else
				{
					return false;
				}
			break;

			case 'permits':
				$query = DB::table('permits_document')->select('permits')->where('permits_id','=',json_decode($docu_id))->get();
				if(!empty($query))
				{
					$old_file = json_decode(get_object_vars($query[0])['permits']);
					File::delete($old_file);
				}
				else
				{
					return false;
				}
			break;

			case 'cpr':
				$query = DB::table('cpr_document')->select('cpr_document')->where('cpr_document_id','=',json_decode($docu_id))->get();
				if(!empty($query))
				{
					$old_file = json_decode(get_object_vars($query[0])['cpr_document']);
					File::delete($old_file);
				}
				else
				{
					return false;
				}
			break;
		}
	}

	public function UpdateDocument(Request $request)
	{	
		$now = Carbon::now();
		$user = Auth::user();
		
		$type = $request['current_document_type'];

		if($type == 'license to operate')
		{
			$docu_type = ucwords($type);
			$date = $request[str_replace(' ','_',$type) . '-expiration'];

			$supplier_name = $request['supplier_name'];
			$supplier_id = DB::table('suppliers')->select('supplier_id')->where('supplier','=',$supplier_name)->get()[0]->supplier_id;
			
			$docu_id = json_encode($request['docu_id']);
			$document_upload = $request->file(str_replace(' ','_',$type) . '-upload');
		}
		else
		{
			$docu_type = ucwords($request['current_document_type']);
			$request['current_document_type'];
			$date = $request[$type . '-expiration'];

			$supplier_name = $request['supplier_name'];
			$supplier_id = DB::table('suppliers')->select('supplier_id')->where('supplier','=',$supplier_name)->get()[0]->supplier_id;
			
			$docu_id = json_encode($request['docu_id']);
			$document_upload = $request->file($type . '-upload');
		}
		$file_name = explode('.',$document_upload[0]->getClientOriginalName())[0];
		$docu_path = $this->MoveFiles($supplier_name,array($document_upload),$docu_type);
		
		switch ($type) {
			case 'philgeps':
				$this->DeleteSupplierDocument($type,$docu_id);
				$update = DB::table('philgeps_document')
						->where('philgeps_id','=',$docu_id)
						->update([
							'philgeps' => json_encode($docu_path),
							'philgeps_expiration' => $date,
							'updated_at' => $now,
						]);
				if($update === 1)
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Philgeps Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Successfully Updated.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Philgeps Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Update Failed.');
				}
			break;
			
			case 'license to operate':
				$this->DeleteSupplierDocument($type,$docu_id);
				$update = DB::table('lto_document')
						->where('lto_id','=',$docu_id)
						->update([
							'lto' => json_encode($docu_path),
							'lto_expiration' => $date,
							'updated_at' => $now,
						]);
				if($update === 1)
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update License To Operate Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Successfully Updated.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update License To Operate Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path)),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Update Failed.');
				}
			break;

			case 'permits':
				$this->DeleteSupplierDocument($type,$docu_id);
				$update = DB::table('permits_document')
						->where('permits_id','=',json_decode($docu_id))
						->update([
							'permits' => json_encode($docu_path[0]),
							'permits_expiration' => $date,
							'updated_at' => $now,
						]);
				if($update === 1)
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Permits Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Successfully Updated.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Permits Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Update Failed.');
				}
			break;

			case 'cpr':
				$this->DeleteSupplierDocument($type,$docu_id);
				$update = DB::table('cpr_document')
						->where('cpr_document_id','=',json_decode($docu_id))
						->update([
							'cpr_document' => json_encode($docu_path[0]),
							'cpr_document_expiration' => $date,
							'updated_at' => $now,
						]);
				if($update === 1)
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Certificate Of Product Registration Document',
						'action_status' => 'Success',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('success', $supplier_name . ' Document Successfully Updated.');
				}
				else
				{
					DB::table('suppliers_logs')->insert(array(
						'user_id' => $user->id,
						'name' => $user->name,
						'role' => $user->role,
						'action' => 'Update Certificate Of Product Registration Document',
						'action_status' => 'Failed',
						'supplier_name' => $supplier_name,
						'supplier_document' => stripslashes(json_encode($docu_path[0])),
						'created_at' => $now,
						'updated_at' => $now
					));
					return Redirect::action('SuppliersController@Suppliers')->with('danger', $supplier_name . ' Document Update Failed.');
				}
			break;
		}
	}

}
