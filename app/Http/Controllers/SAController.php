<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Roles;
use App\RequestingDepartment;
use App\Logs;

use Carbon\Carbon;
use Redirect;

class SAController extends Controller
{
    
	public function Users()
	{
		$users = User::select('name','username','role','created_at')->get();
		return view('pages.sa.sa-dashboard', compact('users'));
	}

	public function Departments()
	{
		$depts = RequestingDepartment::all();
		return view('pages.sa.requesting-departments', compact('depts'));
	}

	public function SaveDepartment(Request $request)
	{
		$now = Carbon::now();
		/* Department Code Creation */
		$ctr = RequestingDepartment::count();

		$dept_name = trim($request['dept-name']);
		$exploded = str_replace("'", "", explode(' ', $dept_name));
		$sep = array();
		foreach ($exploded as $key => $value) {
			$sep[] = strtolower($value);
		}
		array_push($sep, $ctr++);
		$abbv = implode('_', $sep);

		$existing_dept = RequestingDepartment::find($abbv);

		if($existing_dept === null)
		{
			/* Save Created Record */
			$store_dept = new RequestingDepartment();

			$store_dept->dept_id = $abbv;
			$store_dept->department = $dept_name;
			$store_dept->created_at = $now;
			$store_dept->updated_at = $now;

			$store_dept->save();

			if($store_dept->save())
			{
				return Redirect::action('SAController@Departments')->with('success', 'Department Successfully Created.');
			}
			else
			{
				return Redirect::action('SAController@Departments')->with('danger', 'Department Creation Failed. Please Try Again.');
			}
		}
		else
		{
			return Redirect::action('SAController@Departments')->with('danger', 'Department Code already exists. Please add an indetifier.');
		}
	}

	public function DeleteDepartment(Request $request)
	{
		$delete = RequestingDepartment::find($request['dept-id'])->delete();
		if($delete)
		{
			return Redirect::action('SAController@Departments')->with('success', 'Department Successfully Deleted.');
		}
		else
		{
			return Redirect::action('SAController@Departments')->with('danger', 'Department Failed to be Deleted.');
		}
	}

}
