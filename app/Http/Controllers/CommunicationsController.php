<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PRModel;
use App\POModel;
use App\Logs;
use App\RequestingDepartment;

use Auth;
use Session;
use DB;
use Carbon\Carbon;

class CommunicationsController extends Controller
{
    
	public function Index()
	{
		$query = DB::table('pr_table')
					->leftJoin('po_table', 'po_table.pr_number', '=', 'pr_table.pr_number')
					->leftJoin('po_status', 'po_status.po_status_id', '=', 'po_table.po_status')
					->leftJoin('requesting_department', 'requesting_department.dept_id', '=', 'pr_table.requesting_dept')
					->where('pr_table.forwarded_to','=','Communications')
					->get();
		$pr_list = array();
		foreach ($query as $key => $value) {
			$val = get_object_vars($value);
			
			$docu_arr = array();

			if($val['document_path'] === null)
			{
				$docu_arr = null;
			}
			else
			{
				$arr = json_decode($val['document_path']);
				foreach ($arr as $index => $el) {
					$ctr = count(explode('/', $el));
					
					$explode = explode('/', $el);
					$folder = $explode[$ctr - 2];
					
					$file = explode('/', $el);
					$file_name =  $file[$ctr - 1];

					$path = $folder . '/' .$file_name;
					
					$docu_arr[] = [
						'file_path' => 'upload-folder/'.$path,
						'file_name' => $file_name
					];
				}
			}
			

			$pr_list[] = [
				'pr_id' => $val['pr_id'],
				'pr_number' => $val['pr_number'],
				'pr_amount' => $val['pr_amount'],
				'po_number' => $val['po_number'],
				'po_amount' => $val['po_amount'],
				'po_status_id' => $val['po_status_id'],
				'po_status' => $val['po_status'],
				'dept_id' => $val['dept_id'],
				'department' => $val['department'],
				'item_description' => $val['item_description'],
				'received_by' => 'Communications',
				'forwarded_to' => 'Pending',
				'date_received' => $val['date_received'],
				'document' => $docu_arr
			];
		}
		return view('pages.communications.communications-dashboard', compact('pr_list'));
	}

}
