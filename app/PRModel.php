<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PRModel extends Model
{
    protected $table = 'pr_table';
    protected $primaryKey = 'pr_id';

    public $incrementing = false;
}
