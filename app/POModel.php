<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class POModel extends Model
{
    protected $table = 'po_table';
    protected $primaryKey = 'po_id';

    public $incrementing = false;
}
