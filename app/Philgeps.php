<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Philgeps extends Model
{
    protected $table = 'philgeps_document';
    protected $primaryKey = 'philgeps_id';

    public $incrementing = false;
}
