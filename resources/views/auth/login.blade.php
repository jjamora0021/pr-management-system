@extends('layouts.app')

@section('header')
<!-- CSS -->
    <link href="{{ URL::asset('css/snippets/bootstrap-3.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/snippets/form-elements.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/snippets/login-page.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('content')
<!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <div class="col-md-12">
                    <div class="col-md-5 text">
                        <img src="{{ URL::asset('images/logo/antipolo-gov-logo-2.png') }}" alt="Antipolo Government Logo">
                    </div>
                    <div class="col-md-7 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Login to PRMS</h3>
                                <p>Enter your username and password to log on:</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-lock"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form role="form" method="POST" action="{{ url('/login') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="username" class="sr-only">Username</label>
                                    <input id="username" type="text" class="form-control form-username" name="username" value="{{ old('username') }}" placeholder="Username">
                                        @if($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="sr-only">Password</label>
                                    <input id="password" type="password" class="form-control form-password" name="password" placeholder="Password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn">LOGIN</button>
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('register') }}"><button type="button" class="btn btn-warning">REGISTER USER</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ URL::asset('js/snippets/jquery.backstretch.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/snippets/login-page.js') }}"></script>
@endsection
