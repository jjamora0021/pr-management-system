@extends('layouts.app')

@section('header')
	<link href="{{ URL::asset('css/receiving/receiving.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('content')

<!-- Navigation Section -->
@include('pages.receiving.receiving-nav')
<!-- End of Navigation Section -->

<!-- Start of Page -->
<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
        	<!-- Flash Message -->
        	@if(Session::has('success'))
				<div class="alert alert-success">
					<strong>{{ Session::get('success') }}</strong>
				</div>
			@endif
			@if(Session::has('danger'))
				<div class="alert alert-danger">
					<strong>{{ Session::get('danger') }}</strong>
				</div>
			@endif
            <div class="panel panel-primary">
                <div class="panel-heading">Create Purchase Request</div>
	                <div class="panel-body">
	                	{!! Form::open(['action' => 'ReceivingController@SavePR', 'files' => 'true', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'create-pr-form']) !!}
							<div class="form-group">
								<label for="pr_num" class="col-md-4 control-label">PR Number:</label>
	                            <div class="col-md-6">
	                                <input id="pr_num" type="text" class="form-control" name="pr_num" placeholder="Ex: 123456" required>
	                            </div>
							</div>
							<div class="form-group">
								<label for="pr_amount" class="col-md-4 control-label">PR Amount (in Peso):</label>
	                            <div class="col-md-6">
	                                <input id="pr_amount" type="text" class="form-control" name="pr_amount" placeholder="Ex: 1234.56" required>
	                            </div>
							</div>
							<div class="form-group">
								<label for="requesting_dept" class="col-md-4 control-label">Requesting Department</label>
	                            <div class="col-md-6">
	                            	@if(empty($depts))
	                            	<select name="requesting_dept" id="requesting_dept" class="form-control" disabled>
										<option value="" style="display: none;">No Departments Available</option>
									</select>
	                            	@else
	                            	<select name="requesting_dept" id="requesting_dept" class="form-control" required>
										@foreach($depts as $key => $val)
											<option value="" style="display: none;">Select Requesting Department</option>
											<option value="{{ $val->dept_id }}">{{ $val->department }}</option>
										@endforeach
									</select>
	                            	@endif
	                            </div>
							</div>
							<div class="form-group">
								<label for="item_desc" class="col-md-4 control-label">Item Description</label>
	                            <div class="col-md-6">
	                                <input id="item_desc" type="text" class="form-control" name="item_desc" placeholder="Ex: General Merchendise" required>
	                            </div>
							</div>

							<div class="form-group">
								<label for="date_received" class="col-md-4 control-label">Date Received</label>
								<div class="col-md-6">
									<div class="input-group date" data-provide="datepicker">
									    <input type="text" class="form-control" id="date_received" name="date_received" placeholder="Ex: Month / Date / Year" required readonly>
									    <div class="input-group-addon">
									        <span class="glyphicon glyphicon-th"></span>
									    </div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="item_desc" class="col-md-4 control-label" style="margin-bottom: 20px;">Upload Document</label>
								<div class="col-md-8 col-md-offset-2">
									<div class="file-loading">
										<input id="docu-upload" name="docu-upload[]" type="file" multiple>
									</div> 
								</div>
							</div>

	                        <div class="form-group">
	                            <div class="col-md-8 col-md-offset-4">
	                                <button type="submit" class="btn btn-primary pull-right">
	                                    <i class="fas fa-file"></i> Create PR
	                                </button>
	                            </div>
	                        </div>
	                	{!! Form::close() !!}
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
	

@section('functions')
	
	<script type="text/javascript">
		$(document).ready(function() {
			SetNav();
			InitializeFileUpload();
			OnlyInt();
			$('.alert').delay(10000).slideUp(300);
		});

		function SetNav() 
		{
			$('nav #dashboard a').removeClass('active');
			$('nav #create-pr a').addClass('active');
		}

		function InitializeFileUpload() 
		{
			$("#docu-upload").fileinput({
				theme: "explorer",
			    previewFileType: "image",
		        browseClass: "btn btn-warning",
		        browseLabel: "Browse",
		        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
		        removeClass: "btn btn-danger",
		        removeLabel: "Delete",
		        removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
		        maxFileSize: 10000,
		        showUpload: false,
        		maxFileCount: 10,
        		uploadUrl: "{{ url('upload-folder/') }}",
		        allowedFileExtensions: ["jpg", "png", "pdf"]
			});
			$('.fileinput-upload-button').css('display','none');
			$('.kv-file-zoom').addClass('btn-info');
			$('.kv-file-remove').addClass('btn-danger');
		}

		function OnlyInt()
		{
			$("#pr_amount").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });
		}
	</script>

@endsection

@endsection