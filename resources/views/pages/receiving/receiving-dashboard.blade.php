@extends('layouts.app')

@section('header')
	<link href="{{ URL::asset('css/receiving/receiving.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('content')

<!-- Navigation Section -->
@include('pages.receiving.receiving-nav')
<!-- End of Navigation Section -->

<!-- Start of Page -->
<div class="container-fluid">
    <div class="col-md-12">
        <div class="row">
        	<!-- Flash Message -->
	    	@if(Session::has('success'))
				<div class="alert alert-success flash-message">
					<strong>{{ Session::get('success') }}</strong>
				</div>
			@endif
			@if(Session::has('danger'))
				<div class="alert alert-danger flash-message">
					<strong>{{ Session::get('danger') }}</strong>
				</div>
			@endif
            <div class="panel panel-primary">
                <div class="panel-heading">Purchase Request List</div>
	                <div class="panel-body">
	                	<div class="col-md-12 nopadding legend-container">
							<div class="legend col-md-4 nopadding" style="margin-bottom: 15px;">
								<li><b style="font-weight:700;">Legend: </b></li>
							    <li><span class="forwarded"></span> Forwarded</li>
							    <li><span class="received"></span> Received</li>
							    <!-- <li><span class="close_op"></span> Closed</li>
							    <li><span class="shortened"></span> Shortened</li>
							    <li><span class="close_due_to_holiday"></span> Closed due to Holiday</li> -->
							</div>
						</div>
						<table id="receiving-dashboard-table" class="display table-responsive table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th colspan="2">Purchase Request</th>
									<th colspan="3">Purchase Order</th>
									<th rowspan="2">Requesting Department</th>
									<th rowspan="2">Item Description</th>
									<th rowspan="2">Received By</th>
									<th rowspan="2">Forwarded To</th>
									<th rowspan="2">Date Received</th>
									<th rowspan="2">Document<br><small style="color:red">click on the image to enlarge</small></th>
									<th rowspan="2">Action</th>
								</tr>
								<tr>
									<th>PR No.</th>
									<th>PR Amount</th>
									<th>PO No.</th>
									<th>PO Amount</th>
									<th>PO Status</th>
								</tr>
							</thead>
							@if(!empty($pr_list))
							<tbody>
								@foreach($pr_list as $key => $value)
									<tr>
										<td>{{ $value['pr_number'] }}</td>
										<td>{{ $value['pr_amount'] }}</td>
										@if($value['po_status_id'] === 'in_progress')
										<td colspan="3">{{ $value['po_status'] }}</td>
										<td style="display: none;">{{ $value['po_status'] }}</td>
										<td style="display: none;">{{ $value['po_status'] }}</td>
										@else
										<td>{{ $value['po_number'] }}</td>
										<td>{{ $value['po_amount'] }}</td>
										<td>{{ $value['po_status'] }}</td>
										@endif
										<td>{{ $value['department'] }}</td>
										<td>{{ $value['item_description'] }}</td>
										<td>{{ $value['received_by'] }}</td>
										<td>{{ $value['forwarded_to'] }}</td>
										<td>{{ $value['date_received'] }}</td>
										@if($value['document'] === null)
										<td>No Document Uploaded</td>
										@else
										<td>
											<table>
												<tr>
													<td>
														@foreach($value['document'] as $index => $val)
															<?php 
																$file_name = $val['file_name']; 
																$exploded = explode('.', $file_name);
															?>
															@if($exploded[count($exploded) - 1] !== 'pdf')
															<img src="{{ URL::asset('').$val['file_path'] }}" title="{{ $file_name }}" alt="{{ $file_name }}" onclick="ShowImageModal('{{ URL::asset('').$val['file_path'] }}','{{ $file_name }}','{{ $exploded[count($exploded) - 1] }}');">
															@else
															<img src="{{ URL::asset('images/logo/pdf-icon.png') }}" title="{{ $file_name }}" onclick="ShowImageModal('{{ URL::asset('').$val['file_path'] }}','{{ $file_name }}','{{ $exploded[count($exploded) - 1] }}');">
															@endif
														@endforeach
													</td>
												</tr>
											</table>
										</td>
										@endif
										<td>
											<!-- <button class="btn btn-sm btn-info" onclick="ShowUpdateModal('{{ $value['pr_id'] }}')"><i class="fas fa-edit"></i> Update</button> -->
											@if($value['forwarded_to'] === 'Pending')
											<button class="btn btn-sm btn-success" onclick="ForwardPR('{{ $value['pr_id'] }}','{{ $value['pr_number'] }}');"><i class="fas fa-arrow-right"></i> Forward</button>
											<button class="btn btn-sm btn-danger" onclick="VerifyDeletePR('{{ $value['pr_id'] }}','{{ $value['pr_number'] }}');"><i class="fas fa-trash-alt"></i> Delete</button>
											@else
											<button class="btn btn-sm btn-success" disabled><i class="fas fa-arrow-right"></i> Forwarded</button>
											<button class="btn btn-sm btn-danger" disabled><i class="fas fa-trash-alt"></i> Delete</button>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
							@else
							<tbody></tbody>
							@endif
						</table>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Notification Modal -->
<div id="notification-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> <span id="title">Notification</span></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div id="content-container"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
	
<!-- Show Image Modal -->
<div id="show-image-modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<img id="file-img" class="img-responsive" src="" alt="" width="100%">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- PDF Modal -->
<div id="pdf-modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">              
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
    			<h4 class="modal-title"></h4>                  
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<embed id="pdf" src="" type="application/pdf"  height="500px" width="100%">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Delete PR Modal -->
<div id="delete-pr-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> Delete Purchase Request</h4>
			</div>
			<div class="modal-body">
			{!! Form::open(['action' => 'ReceivingController@DeletePR']) !!}
				<input type="hidden" id="pr_id" name="pr_id" value="">
				<h4>Deleting this will permanently remove this Purchase Request from the system. Do you still want to delete this?</h4>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Delete</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="CloseDeleteModal();">Cancel</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@section('functions')
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.alert').delay(10000).slideUp(300);
			InitializeTable();
			SetColorToTableRows();
		});

		function InitializeTable()
		{
			$('#receiving-dashboard-table').dataTable();
			$('.paginate_button').attr('onclick','SetColorToTableRows()');
		}

		function SetColorToTableRows()
		{
			$('#receiving-dashboard-table tbody tr').find('td:nth-child(9)').each (function() {
				if($(this).text() !== 'Pending') 
				{
					$(this).parent().addClass('forwarded_status');
				}
				else
				{
					$(this).parent().addClass('received_status');
				}
			});
		}

		function ShowImageModal(path,file_name,file_type)
		{
			if(file_type === 'pdf')
			{
				$('#pdf-modal').modal({backdrop: 'static', keyboard: false});
				$('#pdf-modal .modal-header').empty().text(file_name);
				$('#pdf-modal #pdf').attr('src',path);
			}
			else
			{
				$('#show-image-modal').modal({backdrop: 'static', keyboard: false});
				$('#show-image-modal .modal-header').empty().text(file_name);
				$('#show-image-modal #file-img').attr('src',path);
			}
		}

		function ForwardPR(pr_id, pr_num)
		{
			$.ajax({
				url : "{{ url('forward-to-admin') }}",
				data : 
				{
					'pr_id' : pr_id,
					'pr_num' : pr_num
				},
				success:function(response)
				{
					if(response !== 'Failed')
					{
						$('#receiving-dashboard-table tbody').empty();
						var dataSet = [];
						$.each(response, function(index, el){
							var url = "{{ url('') }}";
							if(el['forwarded_to'] === 'Admin')
							{
								var btn_set = '<button class="btn btn-sm btn-success" disabled><i class="fas fa-arrow-right"></i> Forwarded</button>\
								<button class="btn btn-sm btn-danger" disabled><i class="fas fa-trash-alt"></i> Delete</button>';
							}
							else
							{
								var forward_pr = "ForwardPR('"+el['pr_id']+"','"+el['pr_number']+"')";
								var delete_pr = "VerifyDeletePR('"+el['pr_id']+"','"+el['pr_number']+"')";
								var btn_set = '<button class="btn btn-sm btn-success" onclick="'+forward_pr+'"><i class="fas fa-arrow-right"></i> Forward</button>\
											<button class="btn btn-sm btn-danger" onclick="'+delete_pr+'"><i class="fas fa-trash-alt"></i> Delete</button>';
							}

							if(el['document'] !== null)
							{
								var td = '';
								$.each(el['document'], function(key, val){
									var split = val['file_name'].split('.');
									var filetype = split[split.length - 1];
									var src = url + '/' + val['file_path'];

									var file_name = val['file_name'];
									var pdf_icon = "{{ URL::asset('images/logo/pdf-icon.png') }}";

									var click_event = "ShowImageModal('"+src+"', '"+file_name+"', '"+filetype+"')";
									if(filetype === 'pdf')
									{
										td += '<img src="'+pdf_icon+'" title="'+file_name+'" onclick="'+click_event+'">';
									}
									else
									{
										td += '<img src="'+src+'" title="'+file_name+'" onclick="'+click_event+'">';
									}
								});
								var docu_set = [ '<table><tr><td>'+td+'</td><tr></table>' ];
							}
							else
							{
								var docu_set = 'No Document Uploaded';
							}

							var rowData = [ el['pr_number'], el['pr_amount'], el['po_number'], el['po_amount'], el['po_status'], el['department'], el['item_description'], el['received_by'], el['forwarded_to'], el['date_received'], docu_set, btn_set ];
							dataSet.push(rowData);
						});
						$('#receiving-dashboard-table').dataTable({
							data : dataSet,
							destroy: true
						});
						$('#receiving-dashboard-table tbody tr').find('td:nth-child(5)').each (function() {
							if($(this).text() === 'In Progress') 
							{
								$(this).parent().find('td:nth-child(5)').attr('colspan','3');
								$(this).parent().find('td:nth-child(4)').remove();
								$(this).parent().find('td:nth-child(3)').remove();
							}
							else if($(this).text() === 'Pending')
							{
								$(this).parent().find('td:nth-child(5)').attr('colspan','3');
								$(this).parent().find('td:nth-child(4)').remove();
								$(this).parent().find('td:nth-child(3)').remove();
							}
						});
						$('#receiving-dashboard-table tbody tr').find('td:nth-child(7)').each (function() {
							if($(this).text() !== 'Pending') 
							{
								$(this).parent().addClass('forwarded_status');
							}
							else
							{
								$(this).parent().addClass('received_status');
							}
						});
						$('.paginate_button').attr('onclick','RecolorTable()');
					}
					else
					{
						alert('error');
					}
				}
			});
		}

		function RecolorTable()
		{
			$('#receiving-dashboard-table tbody tr').find('td:nth-child(7)').each (function() {
				if($(this).text() !== 'Pending') 
				{
					$(this).parent().addClass('forwarded_status');
				}
				else
				{
					$(this).parent().addClass('received_status');
				}
			});
			$('#receiving-dashboard-table tbody tr').find('td:nth-child(5)').each (function() {
				if($(this).text() === 'In Progress') 
				{
					$(this).parent().find('td:nth-child(5)').attr('colspan','3');
					$(this).parent().find('td:nth-child(4)').remove();
					$(this).parent().find('td:nth-child(3)').remove();
				}
				else if($(this).text() === 'Pending')
				{
					$(this).parent().find('td:nth-child(5)').attr('colspan','3');
					$(this).parent().find('td:nth-child(4)').remove();
					$(this).parent().find('td:nth-child(3)').remove();
				}
			});
		}

		function VerifyDeletePR(pr_id,pr_number)
		{
			$('#delete-pr-modal').modal({backdrop: 'static', keyboard: false});
			$('#delete-pr-modal #pr_id').val(pr_id);
		}

		function CloseDeleteModal()
		{
			$('#delete-pr-modal #pr_id').val();
			$('#delete-pr-modal').modal('toggle');
		}
	</script>

@endsection

@endsection