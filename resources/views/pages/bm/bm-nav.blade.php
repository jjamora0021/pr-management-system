<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('receiving-dashboard') }}">BM Dashboard</a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li>
                        <a href="{{ url('login') }}"><i class="fas fa-sign-in-alt"></i> Login</a>
                    </li>
                    <li>
                        <a href="{{ url('register') }}"><i class="fas fa-user"></i> Register</a>
                    </li>
                @else
                    <li>
                        <a href="{{ url('register') }}">Register New User</a>
                    </li>
                    <li class="dropdown">
                        <a href="#"><i class="fas fa-user"></i> {{ Auth::user()->name }}</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>