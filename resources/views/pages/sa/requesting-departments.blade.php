@extends('layouts.app')

@section('header')
	<link href="{{ URL::asset('css/super-admin/sa.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('content')

<!-- Navigation Section -->
@include('pages.sa.sa-nav')
<!-- End of Navigation Section -->

<!-- Start of Page -->
<div class="container">
    <div class="col-md-8 col-md-offset-2">
    	<!-- Flash Message -->
    	@if(Session::has('success'))
			<div class="alert alert-success flash-message"> {{ Session::get('success') }} </div>
		@endif
		@if(Session::has('danger'))
			<div class="alert alert-danger flash-message"> {{ Session::get('danger') }} </div>
		@endif
        <div class="row">
        	<div class="panel with-nav-tabs panel-primary">
        		<!-- Tabs Section -->
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a  href="#dept-list" data-toggle="tab">LIST</a></li>
                            <li class=""><a  href="#add-dept" data-toggle="tab">ADD</a></li>
                        </ul>
                </div>
                <!-- Tab Panel Content -->
                <div class="panel-body">
                    <div class="tab-content">
                    	<!-- List -->
                        <div class="tab-pane in active" id="dept-list">
                        	<table id="requesting-depts-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Code</th>
										<th>Department</th>
										<th>Action</th>
									</tr>
									@if(!empty($depts))
									<tbody>
										@foreach($depts as $key => $val)
										<tr>
											<td>{{ $val->dept_id }}</td>
											<td>{{ $val->department }}</td>
											<td>
												<button class="btn btn-danger btn-xs" onclick="VerifyDeleteAction('{{ $val->dept_id }}');"><i class="fas fa-trash-alt"></i> Delete</button>
											</td>
										</tr>
										@endforeach
									</tbody>
									@else
									<tbody></tbody>
									@endif
								</thead>
                        	</table>
                        </div>
						<!-- Add -->
                        <div class="tab-pane fade" id="add-dept">
                        	{!! Form::open(['action' => 'SAController@SaveDepartment', 'id' => 'add-dept-form', 'class' => 'form-horizontal', 'role' => 'form']) !!}
								<div class="form-group{{ $errors->has('dept-name') ? ' has-error' : '' }}">
									<p class="col-md-8 col-md-offset-3" style="color: red">All Department name should always start with "City". See Example below.</p>
                                    <label for="dept-name" class="col-md-3 control-label">Department</label>

                                    <div class="col-md-6">
                                        <input id="dept-name" type="text" class="form-control" name="dept-name" value="{{ old('dept-name') }}" placeholder="City Treasury" required>
										<div class="alert alert-danger" id="dept-form-error-helper" style="margin-top: 10px; padding: 10px; display: none;">Department must contain "City" in front.</div>
                                        @if($errors->has('dept-name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dept-name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-md-2">
                                    	<input type="submit" id="add-dept-submit" class="btn btn-primary" value="Save" onClick="return ValidateDept();">
                                    </div>
                                </div>
                        	{!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Delete Confirmation Modal -->
<div id="delete-confirmation-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> Delete Department</h4>
			</div>
			{!! Form::open(['id' => 'delete-dept-form', 'role' => 'form', 'action' => 'SAController@DeleteDepartment']) !!}
			<input type="hidden" id="dept-id" name="dept-id" value="">
			<div class="modal-body">
				<h4>Deleting this will permanently remove this DEPARTMENT from the system. Do you still want to delete this?</h4>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Delete</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="CloseDeleteModal();">Cancel</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
	

@section('functions')
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.flash-message').delay(10000).slideUp(300);
			SetNav();
			InitializeTable();
		});

		/* Check If There is a "City" at the beginning */
		function ValidateDept()
		{
			var dept;
		    dept = $('#dept-name').val();
		    if (dept.includes('City')) 
		    {
		    	$('#add-dept-submit').one("click", function() {
		    		return true;
				});
		    }
		    else
		    {
		    	$('#dept-form-error-helper').css('display', 'block');
		    	$('#dept-form-error-helper').delay(10000).slideUp(300);
		    	return false;
		    }
		}

		function SetNav()
		{
			$('#users a').removeClass('active');
			$('#reqesting_dept a').addClass('active')
		}

		function InitializeTable()
		{
			$('#requesting-depts-table').dataTable();
		}

		function VerifyDeleteAction(id)
		{
			$('#delete-confirmation-modal').modal({backdrop: 'static', keyboard: false});
			$('#dept-id').val(id);
		}

		function CloseDeleteModal()
		{
			$('#delete-confirmation-modal #dept-id').val();
			$('#delete-confirmation-modal').modal('toggle');
		}
	</script>

@endsection

@endsection