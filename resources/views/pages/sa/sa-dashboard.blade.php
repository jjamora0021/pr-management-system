@extends('layouts.app')

@section('header')
	<link href="{{ URL::asset('css/super-admin/sa.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('content')

<!-- Navigation Section -->
@include('pages.sa.sa-nav')
<!-- End of Navigation Section -->

<!-- Start of Page -->
<div class="container">
    <div class="col-md-12">
        <div class="row">
        	<div class="panel with-nav-tabs panel-primary">
        		<!-- Tabs Section -->
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a  href="#user-list" data-toggle="tab">LIST</a></li>
                        </ul>
                </div>
                <!-- Tab Panel Content -->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="user-list">
                        	<table id="registered-users-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Full Name</th>
										<th>Username</th>
										<th>Role</th>
										<th>Date Created</th>
									</tr>
									@if(!empty($users))
									<tbody>
										@foreach($users as $key => $val)
										<tr>
											<td>{{ $val->name }}</td>
											<td>{{ $val->username }}</td>
											<td>{{ $val->role }}</td>
											<td>{{ $val->created_at }}</td>
										</tr>
										@endforeach
									</tbody>
									@else
									<tbody></tbody>
									@endif
								</thead>
                        	</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	

@section('functions')
	
	<script type="text/javascript">
		$(document).ready(function() {
			InitializeTable();
		});

		function InitializeTable()
		{
			$('#registered-users-table').dataTable();
		}
	</script>

@endsection

@endsection