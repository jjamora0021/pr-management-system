<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <ul class="nav navbar-nav navbar-right">
                <li id="users"><a class="navbar-brand active" href="{{ url('registered-users') }}">Registered Users</a></li>
                <li id="reqesting_dept"><a class="navbar-brand" href="{{ url('requesting-depts') }}">Requesting Departments</a></li>
            </ul>
            
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li>
                        <a href="{{ url('login') }}"><i class="fas fa-sign-in-alt"></i> Login</a>
                    </li>
                    <li>
                        <a href="{{ url('register') }}"><i class="fas fa-user"></i> Register</a>
                    </li>
                @else
                    <li>
                        <a href="#"><i class="fas fa-user"></i> {{ Auth::user()->name }}</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>