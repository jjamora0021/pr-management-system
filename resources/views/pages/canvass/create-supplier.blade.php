{!! Form::open(['action' => 'SuppliersController@SaveSupplier', 'id' => 'add-supplier-form', 'class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}
	<div class="col-md-12">
		<div class="col-md-6 col-md-offset-3 text-center">
			<div class="form-group">
				<h3>Supplier Info</h3>
			</div>
		</div>
		<div class="col-md-6 col-md-offset-3" id="supplier_info">
			<div class="form-group{{ $errors->has('suppliers-name') ? ' has-error' : '' }}">
		        <label for="suppliers-name" class="col-md-3 control-label">Supplier</label>
		        <div class="col-md-6">
		            <input id="suppliers-name" type="text" class="form-control" name="suppliers-name" value="{{ old('suppliers-name') }}" placeholder="Enter Supplier Name" required onblur="CheckIfExists();">
		            @if($errors->has('suppliers-name'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('suppliers-name') }}</strong>
		                </span>
		            @endif
		        </div>
		        <div class="col-md-3" style="padding: 5px;" id="error-div">
		        </div>
		    </div>

		    <div class="form-group{{ $errors->has('suppliers-address') ? ' has-error' : '' }}">
		        <label for="suppliers-address" class="col-md-3 control-label">Supplier Adress</label>
		        <div class="col-md-6">
		            <input id="suppliers-address" type="text" class="form-control" name="suppliers-address" value="{{ old('suppliers-address') }}" placeholder="Enter Supplier Address" required>
		            @if($errors->has('suppliers-address'))
		                <span class="help-block">
		                    <strong>{{ $errors->first('suppliers-address') }}</strong>
		                </span>
		            @endif
		        </div>
		    </div>

		    <div class="form-group" id="mobile-number-container">
		        <div class="col-md-12 mobile-lane-1 nopadding">
		        	<label for="suppliers-mobile-number" class="col-md-3 control-label">Mobile Number</label>
				        <div class="col-md-4 mobile-number-lane-1">
				            <input id="suppliers-mobile-number-1" type="text" class="form-control suppliers-mobile" name="suppliers-mobile-number[]" placeholder="Enter Supplier Mobile Number 1" onkeypress="EnablePlusMobile(1);" maxlength="11">
				        </div>
				        <div class="col-md-2">
							<button type="button" class="btn btn-success" onclick="AddMobileNumber(1);" id="add-mobile-btn-1" disabled><i class="fas fa-plus"></i></button>
						</div>
		        </div>
		    </div>

		    <div class="form-group" id="phone-number-container">
		        <div class="col-md-12 phone-lane-1 nopadding">
		        	<label for="suppliers-phone-number" class="col-md-3 control-label">Phone Number</label>
				        <div class="col-md-4 phone-number-lane-1">
				            <input id="suppliers-phone-number-1" type="text" class="form-control suppliers-phone" name="suppliers-phone-number[]" placeholder="Enter Supplier Phone Number 1" onkeypress="EnablePlusPhone(1);" maxlength="9">
				        </div>
				        <div class="col-md-2">
							<button type="button" class="btn btn-success" onclick="AddPhoneNumber(1);" id="add-phone-btn-1" disabled><i class="fas fa-plus"></i></button>
						</div>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-6 col-md-offset-3 text-center">
			<div class="form-group">
				<h3>Supplier Documents</h3>
			</div>
		</div>
	    <div class="col-md-8 col-md-offset-2" id="supplier_documents">
	    	<div class="form-group">
				<div class="col-md-12 nopadding">
					<div class="col-md-12 nopadding">
						<label for="philgeps" class="col-md-12 control-label" style="margin-bottom: 20px;">Philegeps</label>
					</div>
					<div class="philgeps_lane">
						<div class="col-md-12 nopadding lane" id="philgeps-lane-1">
							<div class="col-md-1 nopadding text-center">
								<span id="philgeps-numbering-1" class="numbering badge">1</span>
							</div>
							<div class="col-md-5 nopadding-left">
								<div class="file-loading">
									<input id="philgeps-upload-1" name="philgeps-upload[]" type="file" class="upload" onChange="ValidateHasFile('philgeps','1');">
								</div> 
							</div>
							<div class="col-md-4">
								<div class="input-group date">
								    <input type="text" class="form-control" id="philgeps-expiration-1" name="philgeps-expiration[]" placeholder="Expiration Date" readonly onChange="CheckIfEmpty('philgeps','1');">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-success" onclick="AddPhilgeps(1);" id="add-philgeps-btn-1" disabled><i class="fas fa-plus"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12 nopadding">
					<div class="col-md-12 nopadding">
						<label for="lto" class="col-md-12 control-label" style="margin-bottom: 20px;">License To Operate</label>
					</div>
					<div class="lto_lane">
						<div class="col-md-12 nopadding" id="lto-lane-1">
							<div class="col-md-1 nopadding text-center">
								<span id="lto-numbering-1" class="numbering badge">1</span>
							</div>
							<div class="col-md-5 nopadding-left">
								<div class="file-loading">
									<input id="lto-upload-1" name="lto-upload[]" type="file" class="upload" onChange="ValidateHasFile('lto','1');">
								</div> 
							</div>
							<div class="col-md-4">
								<div class="input-group date">
								    <input type="text" class="form-control" id="lto-expiration-1" name="lto-expiration[]" placeholder="Expiration Date" readonly onChange="CheckIfEmpty('lto','1');">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-success" onclick="AddLTO(1);" id="add-lto-btn-1" disabled><i class="fas fa-plus"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12 nopadding">
					<div class="col-md-12 nopadding">
						<label for="permit" class="col-md-12 control-label" style="margin-bottom: 20px;">Permit</label>
					</div>
					<div class="permit_lane">
						<div class="col-md-12 nopadding" id="permit-lane-1">
							<div class="col-md-1 nopadding text-center">
								<span id="permit-numbering-1" class="numbering badge">1</span>
							</div>
							<div class="col-md-5 nopadding-left">
								<div class="file-loading">
									<input id="permit-upload-1" name="permit-upload[]" type="file" class="upload" onChange="ValidateHasFile('permit','1');">
								</div> 
							</div>
							<div class="col-md-4">
								<div class="input-group date">
								    <input type="text" class="form-control" id="permit-expiration-1" name="permit-expiration[]" placeholder="Expiration Date" readonly onChange="CheckIfEmpty('permit','1');">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-success" onclick="AddPermit(1);" id="add-permit-btn-1" disabled><i class="fas fa-plus"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12 nopadding">
					<div class="col-md-12 nopadding">
						<label for="permit" class="col-md-12 control-label" style="margin-bottom: 20px;">Certificate Of Product Registration <span class="expired_status">(for medical supplies only)</span></label>
					</div>
					<div class="cpr_lane">
						<div class="col-md-12 nopadding" id="cpr-lane-1">
							<div class="col-md-1 nopadding text-center">
								<span id="permit-numbering-1" class="numbering badge">1</span>
							</div>
							<div class="col-md-5 nopadding-left">
								<div class="file-loading">
									<input id="cpr-upload-1" name="cpr-upload[]" type="file" class="upload" onChange="ValidateHasFile('cpr','1');">
								</div> 
							</div>
							<div class="col-md-4">
								<div class="input-group date">
								    <input type="text" class="form-control" id="cpr-expiration-1" name="cpr-expiration[]" placeholder="Expiration Date" readonly onChange="CheckIfEmpty('cpr','1');">
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-success" onclick="AddCPR(1);" id="add-cpr-btn-1" disabled><i class="fas fa-plus"></i></button>
							</div>
						</div>
					</div>
				</div>	
			</div>
	    </div>
    </div>

		<div class="col-md-10" style="padding-right: 8px !important;">
			<input type="submit" id="add-dept-submit" class="btn btn-primary pull-right" value="Save">
		</div>
	</div>
{!! Form::close() !!}