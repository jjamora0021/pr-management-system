@extends('layouts.app')

@section('header')
	<link href="{{ URL::asset('css/pr-canvass/pr-canvass.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('content')

<!-- Navigation Section -->
@include('pages.canvass.pr-canvass-nav')
<!-- End of Navigation Section -->

<!-- Start of Page -->
<div class="container-fluid">
    <div class="col-md-12">
        <div class="row">
        	<!-- Flash Message -->
	    	@if(Session::has('success'))
				<div class="alert alert-success flash-message">
					<strong>{{ Session::get('success') }}</strong>
				</div>
			@endif
			@if(Session::has('danger'))
				<div class="alert alert-danger flash-message">
					<strong>{{ Session::get('danger') }}</strong>
				</div>
			@endif
            <div class="panel panel-primary">
                <div class="panel-heading">Purchase Request List</div>
	                <div class="panel-body">
	                	<div class="col-md-12 nopadding legend-container">
							<div class="legend col-md-12 nopadding" style="margin-bottom: 15px;">
								<li><b style="font-weight:700;">Legend: </b></li>
							    <!-- <li><span class="communications"></span> Communications</li>
							    <li><span class="medical_supplies"></span> For Canvas (Medical Supplies)</li>
							    <li><span class="general_merchendise"></span> For Canvas (General Merchendise)</li> -->
							    <li><span class="received"></span> Received</li>
							</div>
						</div>
						<table id="communications-dashboard-table" class="display table-responsive table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th colspan="2">Purchase Request</th>
									<th colspan="3">Purchase Order</th>
									<th rowspan="2">Requesting Department</th>
									<th rowspan="2">Item Description</th>
									<th rowspan="2">Received By</th>
									<th rowspan="2">Forwarded To</th>
									<th rowspan="2">Date Received</th>
									<th rowspan="2">Document<br><small style="color:red">click on the image to enlarge</small></th>
									<th rowspan="2">Action</th>
								</tr>
								<tr>
									<th>PR No.</th>
									<th>PR Amount</th>
									<th>PO No.</th>
									<th>PO Amount</th>
									<th>PO Status</th>
								</tr>
							</thead>
							@if(!empty($pr_list))
							<tbody>
								@foreach($pr_list as $key => $value)
									<tr>
										<td>{{ $value['pr_number'] }}</td>
										<td>{{ $value['pr_amount'] }}</td>
										@if($value['po_status_id'] === 'in_progress')
										<td colspan="3">{{ $value['po_status'] }}</td>
										<td style="display: none;">{{ $value['po_status'] }}</td>
										<td style="display: none;">{{ $value['po_status'] }}</td>
										@else
										<td>{{ $value['po_number'] }}</td>
										<td>{{ $value['po_amount'] }}</td>
										<td>{{ $value['po_status'] }}</td>
										@endif
										<td>{{ $value['department'] }}</td>
										<td>{{ $value['item_description'] }}</td>
										<td>{{ $value['received_by'] }}</td>
										<td>{{ $value['forwarded_to'] }}</td>
										<td>{{ $value['date_received'] }}</td>
										@if($value['document'] === null)
										<td>No Document Uploaded</td>
										@else
										<td>
											<table>
												<tr>
													<td>
														@foreach($value['document'] as $index => $val)
															<?php 
																$file_name = $val['file_name']; 
																$exploded = explode('.', $file_name);
															?>
															@if($exploded[count($exploded) - 1] !== 'pdf')
															<img src="{{ URL::asset('').$val['file_path'] }}" title="{{ $file_name }}" alt="{{ $file_name }}" onclick="ShowImageModal('{{ URL::asset('').$val['file_path'] }}','{{ $file_name }}','{{ $exploded[count($exploded) - 1] }}');">
															@else
															<img src="{{ URL::asset('images/logo/pdf-icon.png') }}" title="{{ $file_name }}" onclick="ShowImageModal('{{ URL::asset('').$val['file_path'] }}','{{ $file_name }}','{{ $exploded[count($exploded) - 1] }}');">
															@endif
														@endforeach
													</td>
												</tr>
											</table>
										</td>
										@endif
										<td>
											@if($value['received_by'] === 'Communications')
											<button class="btn btn-sm btn-success" onchange="ForwardedToBM('{{ $value['pr_id'] }}');"><i class="fas fa-arrow-right"></i> Forward To BM</button>
											@else
											<button class="btn btn-sm btn-success" disabled><i class="fas fa-arrow-right"></i> Forward To BM</button>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
							@else
							<tbody></tbody>
							@endif
						</table>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Notification Modal -->
<div id="notification-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> <span id="title">Notification</span></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div id="content-container"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
	
<!-- Show Image Modal -->
<div id="show-image-modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<img id="file-img" class="img-responsive" src="" alt="" width="100%">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- PDF Modal -->
<div id="pdf-modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">              
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
    			<h4 class="modal-title"></h4>                  
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<embed id="pdf" src="" type="application/pdf"  height="500px" width="100%">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


@section('functions')
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.alert').delay(10000).slideUp(300);
			InitializeTable();
		});

		function InitializeTable()
		{
			$('#communications-dashboard-table').dataTable();
			SetColorToTableRows();
		}

		function ShowImageModal(path,file_name,file_type)
		{
			if(file_type === 'pdf')
			{
				$('#pdf-modal').modal({backdrop: 'static', keyboard: false});
				$('#pdf-modal .modal-header').empty().text(file_name);
				$('#pdf-modal #pdf').attr('src',path);
			}
			else
			{
				$('#show-image-modal').modal({backdrop: 'static', keyboard: false});
				$('#show-image-modal .modal-header').empty().text(file_name);
				$('#show-image-modal #file-img').attr('src',path);
			}
		}

		function SetColorToTableRows()
		{
			$('#communications-dashboard-table tbody tr').find('td:nth-child(9)').each (function() {
				if($(this).text() === 'Pending') 
				{
					$(this).parent().addClass('received_status');
				}
			});
		}

		// function ForwardedTo(pr_id, pr_num)
		// {
		// 	var selected_id = $('#forwarded_to').val();
		// 	var selected_text = $('#forwarded_to option:selected').text();
			
		// 	$.ajax({
		// 		url: "{{ url('forward-to') }}",
		// 		data: 
		// 		{
		// 			'pr_id' : pr_id,
		// 			'pr_num' : pr_num,
		// 			'forwarded_to_id' : selected_id,
		// 			'forwarded_to_text' : selected_text
		// 		},
		// 		success:function(response)
		// 		{
		// 			if(response !== 'Failed')
		// 			{
		// 				$('#admin-dashboard-table tbody').empty();
		// 				var dataSet = [];
		// 				$.each(response, function(index, el){
		// 					var url = "{{ url('') }}";
		// 					if(el['forwarded_to'] !== 'Pending')
		// 					{
		// 						var btn_set = [ '<select name="forwarded_to" id="forwarded_to" class="btn btn-sm btn-success" disabled>\
		// 										<option value="" style="display: none;">Forward To</option>\
		// 									</select>' ];
		// 					}
		// 					else
		// 					{
		// 						var change_event = "ForwardedTo('"+el['pr_id']+"','"+el['pr_number']+"');";
		// 						var btn_set = [ '<select name="forwarded_to" id="forwarded_to" class="form-control" onchange="'+change_event+'" style="text-align: center;">\
		// 										<option value="" style="display: none;">Forward To</option>\
		// 										<option value="communications">Communications</option>\
		// 										<option value="pr_for_canvas_med_supp">For Canvas Medical Supplies</option>\
		// 										<option value="pr_for_canvas_gen_merch">For Canvas General Merchendise</option>\
		// 									</select>' ];
		// 					}

		// 					if(el['document'] !== null)
		// 					{
		// 						var td = '';
		// 						$.each(el['document'], function(key, val){
		// 							var split = val['file_name'].split('.');
		// 							var filetype = split[split.length - 1];
		// 							var src = url + '/' + val['file_path'];

		// 							var file_name = val['file_name'];
		// 							var pdf_icon = "{{ URL::asset('images/logo/pdf-icon.png') }}";

		// 							var click_event = "ShowImageModal('"+src+"', '"+file_name+"', '"+filetype+"')";
		// 							if(filetype === 'pdf')
		// 							{
		// 								td += '<img src="'+pdf_icon+'" title="'+file_name+'" onclick="'+click_event+'">';
		// 							}
		// 							else
		// 							{
		// 								td += '<img src="'+src+'" title="'+file_name+'" onclick="'+click_event+'">';
		// 							}
		// 						});
		// 						var docu_set = [ '<table><tr><td>'+td+'</td><tr></table>' ];
		// 					}
		// 					else
		// 					{
		// 						var docu_set = 'No Document Uploaded';
		// 					}

		// 					var rowData = [ el['pr_number'], el['pr_amount'], el['po_number'], el['po_amount'], el['po_status'], el['department'], el['item_description'], el['received_by'], el['forwarded_to'], el['date_received'], docu_set, btn_set ];
		// 					dataSet.push(rowData);
		// 				});
		// 				console.log(dataSet)
		// 				$('#admin-dashboard-table').dataTable({
		// 					data : dataSet,
		// 					destroy: true
		// 				});
		// 				$('#admin-dashboard-table tbody tr').find('td:nth-child(5)').each (function() {
		// 					if($(this).text() === 'In Progress') 
		// 					{
		// 						$(this).parent().find('td:nth-child(5)').attr('colspan','3');
		// 						$(this).parent().find('td:nth-child(4)').remove();
		// 						$(this).parent().find('td:nth-child(3)').remove();
		// 					}
		// 					else if($(this).text() === 'Pending')
		// 					{
		// 						$(this).parent().find('td:nth-child(5)').attr('colspan','3');
		// 						$(this).parent().find('td:nth-child(4)').remove();
		// 						$(this).parent().find('td:nth-child(3)').remove();
		// 					}
		// 				});
		// 				$('#admin-dashboard-table tbody tr').find('td:nth-child(7)').each (function() {
		// 					if($(this).text() === 'Pending') 
		// 					{
		// 						$(this).parent().addClass('received_status');
		// 					}
		// 				});
		// 				$('.paginate_button').attr('onclick','SetColorToTableRows()');
		// 			}
		// 			else
		// 			{
		// 				alert('error');
		// 			}
		// 		}
		// 	});

		// }
	</script>

@endsection

@endsection