@extends('layouts.app')

@section('header')
	<link href="{{ URL::asset('css/pr-canvass/pr-canvass.css') }}" rel="stylesheet" type="text/css">
	<style>
		td {
			padding: 5px;
		}
	</style>
@endsection

@section('content')

<!-- Navigation Section -->
@include('pages.canvass.pr-canvass-nav')
<!-- End of Navigation Section -->

<!-- Start of Page -->
<div class="container-fluid">
    <div class="col-md-12">
        <div class="row">
        	<!-- Flash Message -->
	    	@if(Session::has('success'))
				<div class="alert alert-success flash-message">
					<strong>{{ Session::get('success') }}</strong>
				</div>
			@endif
			@if(Session::has('danger'))
				<div class="alert alert-danger flash-message">
					<strong>{{ Session::get('danger') }}</strong>
				</div>
			@endif
            <div class="panel panel-primary with-nav-tabs">
                <div class="panel-heading">
                	<ul class="nav nav-tabs">
                        <li class="active"><a  href="#suppliers-list" data-toggle="tab">Suppliers List</a></li>
                        <li class=""><a  href="#add-suppliers" data-toggle="tab">Add Supplier</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                	<div class="tab-content">
            			<div class="tab-pane in active" id="suppliers-list">
							<div class="col-md-12 nopadding legend-container">
								<div class="legend col-md-12 nopadding" style="margin-bottom: 15px;">
									<li><b style="font-weight:700;">Legend: </b></li>
								    <li><span class="expired"></span> Has Document that will expire</li>
								</div>
							</div>
							<table id="suppliers-dashboard-table" class="display table-responsive table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Supplier Name</th>
										<th>Supplier Address</th>
										<th>Mobile Number</th>
										<th>Phone Number</th>
										<th colspan="2">Philgeps</th>
										<th colspan="2">Permit</th>
										<th colspan="2">License To Operate</th>
										<th colspan="2">Certificate of Product Registration</th>
										<th rowspan="2">Action</th>
									</tr>
								</thead>
								@if(!empty($suppliers))
									<tbody>
										@foreach($suppliers as $key => $value)
											<tr id="{{ $value['supplier_id'] }}">
												<td>{{ $value['supplier'] }}</td>
												<td>{{ $value['address'] }}</td>
												<td>
													<table>
														@foreach($value['mobile_number'] as $index => $val)
															<?php $index++; ?>
															<tr><td>{{ $val[$index] }}</td></tr>
														@endforeach
													</table>
												</td>
												<td>
													<table>
														@foreach($value['phone_number'] as $index => $val)
															<?php $index++; ?>
															<tr><td>{{ $val[$index] }}</td></tr>
														@endforeach
													</table>
												</td>
												<td>
													@foreach($value['philgeps'] as $index => $val)
														<table>
															<tr id="{{ $val['philgeps_id'] }}">
																@if($val['philgeps'] == 'Processing')
																	<td class="processing_status">Processing</td>
																@else
																	<td><button class="btn btn-sm btn-default" onclick="ShowDocument('{{ $val['philgeps'] }}','{{ $val['philgeps_id'] }}');">View Document</button></td>
																	<td class="exp_date">{{ $val['philgeps_expiration'] }}</td>
																@endif
															</tr>
														</table>
													@endforeach
												</td>
												<td>
													@foreach($value['lto'] as $index => $val)
														<table>
															<tr id="{{ $val['lto_id'] }}">
																@if($val['lto'] == 'Processing')
																	<td>Processing</td>
																@else
																	<td><button class="btn btn-sm btn-default" onclick="ShowDocument('{{ $val['lto'] }}','{{ $val['lto_id'] }}');">View Document</button></td>
																	<td>{{ $val['lto_expiration'] }}</td>
																@endif
															</tr>
														</table>
													@endforeach
												</td>
											</tr>
										@endforeach
									</tbody>
								@else
									<tbody></tbody>
								@endif
							</table>
						</div>
						<div class="tab-pane fade" id="add-suppliers">
							@include('pages.canvass.create-supplier')
						</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Notification Modal -->
<div id="notification-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> <span id="title">Notification</span></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div id="content-container"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
	
<!-- Show Image Modal -->
<div id="show-image-modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<img id="file-img" class="img-responsive" src="" alt="" width="100%">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- PDF Modal -->
<div id="pdf-modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">              
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
    			<h4 class="modal-title"></h4>                  
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<embed id="pdf" src="" type="application/pdf"  height="500px" width="100%">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


@section('functions')
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.alert').delay(10000).slideUp(300);
			InitializeTable();
			SetNav();
			InitializeFileUpload();
			AcceptOnlyNumbers();
		});


		function InitializeFileUpload() 
		{
			$(".upload").fileinput({
				theme: "explorer",
			    previewFileType: "image",
		        browseClass: "btn btn-warning",
		        browseLabel: "Browse",
		        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
		        removeClass: "btn btn-danger",
		        removeLabel: "Delete",
		        removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
		        maxFileSize: 10000,
		        showUpload: false,
        		maxFileCount: 10,
		        allowedFileExtensions: ["jpg", "png", "pdf"]
			});
			$('.fileinput-upload-button').css('display','none');
			$('.kv-file-zoom').addClass('btn-info');
			$('.kv-file-remove').addClass('btn-danger');
		}

		function SetNav()
		{
			$('#suppliers a').addClass('active');
			$('#canvass-dashboard a').removeClass('active');
		}

		function InitializeTable()
		{
			$('#suppliers-dashboard-table').dataTable();
			SetColorToTableRows();
		}

		function ShowImageModal(path,file_name,file_type)
		{
			if(file_type === 'pdf')
			{
				$('#pdf-modal').modal({backdrop: 'static', keyboard: false});
				$('#pdf-modal .modal-header').empty().text(file_name);
				$('#pdf-modal #pdf').attr('src',path);
			}
			else
			{
				$('#show-image-modal').modal({backdrop: 'static', keyboard: false});
				$('#show-image-modal .modal-header').empty().text(file_name);
				$('#show-image-modal #file-img').attr('src',path);
			}
		}

		function SetColorToTableRows()
		{
			$('#suppliers-dashboard-table tbody tr').find('td:nth-child(9)').each (function() {
				if($(this).text() === 'Pending') 
				{
					$(this).parent().addClass('received_status');
				}
			});
		}

		/* Philgeps Section */
		function AddPhilgeps(ctr)
		{
			$('#add-philgeps-btn-'+ctr).attr('disabled',true);
			ctr++;
			if(ctr != 2)
			{
				var onchange_event = "ValidateHasFile('philgeps','"+ctr+"')";
				var check = "CheckIfEmpty('philgeps','"+ctr+"');"
				var philgeps_lane = '<div class="col-md-12 nopadding lane" id="philgeps-lane-'+ctr+'" style="padding-top: 20px !important;">\
										<div class="col-md-1 nopadding text-center">\
											<span id="philgeps-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
										</div>\
										<div class="col-md-5 nopadding-left">\
											<div class="file-loading">\
												<input id="philgeps-upload-'+ctr+'" name="philgeps-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
											</div> \
										</div>\
										<div class="col-md-4">\
											<div class="input-group date" data-provide="datepicker">\
											    <input type="text" class="form-control" id="philgeps-expiration-'+ctr+'" name="philgeps-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
											    <div class="input-group-addon">\
											        <span class="glyphicon glyphicon-th"></span>\
											    </div>\
											</div>\
										</div>\
										<div class="col-md-2">\
											<button type="button" class="btn btn-success" onclick="AddPhilgeps('+ctr+');" id="add-philgeps-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
											<button type="button" class="btn btn-danger" onclick="RemovePhilgeps('+ctr+');" id="remove-philgeps-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
										</div>\
									</div>';
			}
			else
			{
				var onchange_event = "ValidateHasFile('philgeps','"+ctr+"')";
				var check = "CheckIfEmpty('philgeps','"+ctr+"');"
				var philgeps_lane = '<div class="col-md-12 nopadding lane" id="philgeps-lane-'+ctr+'" style="padding-top: 10px !important;">\
										<div class="col-md-1 nopadding text-center">\
											<span id="philgeps-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
										</div>\
										<div class="col-md-5 nopadding-left">\
											<div class="file-loading">\
												<input id="philgeps-upload-'+ctr+'" name="philgeps-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
											</div> \
										</div>\
										<div class="col-md-4">\
											<div class="input-group date" data-provide="datepicker">\
											    <input type="text" class="form-control" id="philgeps-expiration-'+ctr+'" name="philgeps-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
											    <div class="input-group-addon">\
											        <span class="glyphicon glyphicon-th"></span>\
											    </div>\
											</div>\
										</div>\
										<div class="col-md-2">\
											<button type="button" class="btn btn-success" onclick="AddPhilgeps('+ctr+');" id="add-philgeps-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
											<button type="button" class="btn btn-danger" onclick="RemovePhilgeps('+ctr+');" id="remove-philgeps-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
										</div>\
									</div>';
			}
			$('.philgeps_lane').append(philgeps_lane);
			InitializeFileUpload();
		}

		function RemovePhilgeps(ctr) 
		{
			var prev = ctr - 1;
			$('#add-philgeps-btn-'+prev).attr('disabled',false);
			$('#philgeps-lane-'+ctr).remove();
		}
		/* End of Philgeps Section */

		/* LTO Section */
		function AddLTO(ctr)
		{
			$('#add-lto-btn-'+ctr).attr('disabled',true);
			ctr++;
			if(ctr != 2)
			{
				var onchange_event = "ValidateHasFile('lto','"+ctr+"')";
				var check = "CheckIfEmpty('lto','"+ctr+"');"
				var lto_lane = '<div class="col-md-12 nopadding lane" id="lto-lane-'+ctr+'" style="padding-top: 20px !important;">\
									<div class="col-md-1 nopadding text-center">\
										<span id="lto-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
									</div>\
									<div class="col-md-5 nopadding-left">\
										<div class="file-loading">\
											<input id="lto-upload-'+ctr+'" name="lto-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
										</div> \
									</div>\
									<div class="col-md-4">\
										<div class="input-group date" data-provide="datepicker">\
										    <input type="text" class="form-control" id="lto-expiration-'+ctr+'" name="lto-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
										    <div class="input-group-addon">\
										        <span class="glyphicon glyphicon-th"></span>\
										    </div>\
										</div>\
									</div>\
									<div class="col-md-2">\
										<button type="button" class="btn btn-success" onclick="AddLTO('+ctr+');" id="add-lto-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
										<button type="button" class="btn btn-danger" onclick="RemoveLTO('+ctr+');" id="remove-lto-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
									</div>\
								</div>';
			}
			else
			{
				var onchange_event = "ValidateHasFile('lto','"+ctr+"')";
				var check = "CheckIfEmpty('lto','"+ctr+"');"
				var lto_lane = '<div class="col-md-12 nopadding lane" id="lto-lane-'+ctr+'" style="padding-top: 10px !important;">\
									<div class="col-md-1 nopadding text-center">\
										<span id="lto-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
									</div>\
									<div class="col-md-5">\
										<div class="file-loading">\
											<input id="lto-upload-'+ctr+'" name="lto-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
										</div> \
									</div>\
									<div class="col-md-4">\
										<div class="input-group date" data-provide="datepicker">\
										    <input type="text" class="form-control" id="lto-expiration-'+ctr+'" name="lto-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
										    <div class="input-group-addon">\
										        <span class="glyphicon glyphicon-th"></span>\
										    </div>\
										</div>\
									</div>\
									<div class="col-md-2">\
										<button type="button" class="btn btn-success" onclick="AddLTO('+ctr+');" id="add-lto-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
										<button type="button" class="btn btn-danger" onclick="RemoveLTO('+ctr+');" id="remove-lto-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
									</div>\
								</div>';
			}
			$('.lto_lane').append(lto_lane);
			InitializeFileUpload();
		}

		function RemoveLTO(ctr) 
		{
			var prev = ctr - 1;
			$('#add-lto-btn-'+prev).attr('disabled',false);
			$('#lto-lane-'+ctr).remove();
		}
		/* End of LTO Section */

		/* permit Section */
		function AddPermit(ctr)
		{
			$('#add-permit-btn-'+ctr).attr('disabled',true);
			ctr++;
			if(ctr != 2)
			{
				var onchange_event = "ValidateHasFile('permit','"+ctr+"')";
				var check = "CheckIfEmpty('permit','"+ctr+"');"
				var permit_lane = '<div class="col-md-12 nopadding lane" id="permit-lane-'+ctr+'" style="padding-top: 20px !important;">\
									<div class="col-md-1 nopadding text-center">\
										<span id="permit-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
									</div>\
									<div class="col-md-5 nopadding-left">\
										<div class="file-loading">\
											<input id="permit-upload-'+ctr+'" name="permit-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
										</div> \
									</div>\
									<div class="col-md-4">\
										<div class="input-group date" data-provide="datepicker">\
										    <input type="text" class="form-control" id="permit-expiration-'+ctr+'" name="permit-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
										    <div class="input-group-addon">\
										        <span class="glyphicon glyphicon-th"></span>\
										    </div>\
										</div>\
									</div>\
									<div class="col-md-2">\
										<button type="button" class="btn btn-success" onclick="AddPermit('+ctr+');" id="add-permit-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
										<button type="button" class="btn btn-danger" onclick="RemovePermit('+ctr+');" id="remove-permit-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
									</div>\
								</div>';
			}
			else
			{
				var onchange_event = "ValidateHasFile('permit','"+ctr+"')";
				var check = "CheckIfEmpty('permit','"+ctr+"');"
				var permit_lane = '<div class="col-md-12 nopadding lane" id="permit-lane-'+ctr+'" style="padding-top: 10px !important;">\
									<div class="col-md-1 nopadding text-center">\
										<span id="permit-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
									</div>\
									<div class="col-md-5 nopadding-left">\
										<div class="file-loading">\
											<input id="permit-upload-'+ctr+'" name="permit-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
										</div> \
									</div>\
									<div class="col-md-4">\
										<div class="input-group date" data-provide="datepicker">\
										    <input type="text" class="form-control" id="permit-expiration-'+ctr+'" name="permit-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
										    <div class="input-group-addon">\
										        <span class="glyphicon glyphicon-th"></span>\
										    </div>\
										</div>\
									</div>\
									<div class="col-md-2">\
										<button type="button" class="btn btn-success" onclick="AddPermit('+ctr+');" id="add-permit-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
										<button type="button" class="btn btn-danger" onclick="RemovePermit('+ctr+');" id="remove-permit-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
									</div>\
								</div>';
			}
			$('.permit_lane').append(permit_lane);
			InitializeFileUpload();
		}

		function RemovePermit(ctr) 
		{
			var prev = ctr - 1;
			$('#add-permit-btn-'+prev).attr('disabled',false);
			$('#permit-lane-'+ctr).remove();
		}
		/* End of permit Section */

		/* CPR Section */
		function AddCPR(ctr)
		{
			$('#add-cpr-btn-'+ctr).attr('disabled',true);
			ctr++;
			if(ctr != 2)
			{
				var onchange_event = "ValidateHasFile('cpr','"+ctr+"')";
				var check = "CheckIfEmpty('cpr','"+ctr+"');"
				var cpr_lane = '<div class="col-md-12 nopadding lane" id="lane-'+ctr+'" style="padding-top: 20px !important;">\
								<div class="col-md-1 nopadding text-center">\
									<span id="cpr-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
								</div>\
								<div class="col-md-5 nopadding-left">\
									<div class="file-loading">\
										<input id="cpr-upload-'+ctr+'" name="cpr-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
									</div> \
								</div>\
								<div class="col-md-4">\
									<div class="input-group date" data-provide="datepicker">\
									    <input type="text" class="form-control" id="cpr-expiration-'+ctr+'" name="cpr-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
									    <div class="input-group-addon">\
									        <span class="glyphicon glyphicon-th"></span>\
									    </div>\
									</div>\
								</div>\
								<div class="col-md-2">\
									<button type="button" class="btn btn-success" onclick="AddCPR('+ctr+');" id="add-cpr-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
									<button type="button" class="btn btn-danger" onclick="RemoveCPR('+ctr+');" id="remove-cpr-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
								</div>\
							</div>';	
			}
			else
			{
				var onchange_event = "ValidateHasFile('cpr','"+ctr+"')";
				var check = "CheckIfEmpty('cpr','"+ctr+"');"
				var cpr_lane = '<div class="col-md-12 nopadding lane" id="lane-'+ctr+'" style="padding-top: 10px !important;">\
								<div class="col-md-1 nopadding text-center">\
									<span id="cpr-numbering-'+ctr+'" class="numbering badge">'+ctr+'</span>\
								</div>\
								<div class="col-md-5 nopadding-left">\
									<div class="file-loading">\
										<input id="cpr-upload-'+ctr+'" name="cpr-upload[]" type="file" class="upload" onChange="'+onchange_event+'">\
									</div> \
								</div>\
								<div class="col-md-4">\
									<div class="input-group date" data-provide="datepicker">\
									    <input type="text" class="form-control" id="cpr-expiration-'+ctr+'" name="cpr-expiration[]" placeholder="Expiration Date" readonly onChange="'+check+'">\
									    <div class="input-group-addon">\
									        <span class="glyphicon glyphicon-th"></span>\
									    </div>\
									</div>\
								</div>\
								<div class="col-md-2">\
									<button type="button" class="btn btn-success" onclick="AddCPR('+ctr+');" id="add-cpr-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
									<button type="button" class="btn btn-danger" onclick="RemoveCPR('+ctr+');" id="remove-cpr-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
								</div>\
							</div>';	
			}
			$('.cpr_lane').append(cpr_lane);
			InitializeFileUpload();
		}	

		function RemoveCPR(ctr) 
		{
			var prev = ctr - 1;
			$('#add-cpr-btn-'+prev).attr('disabled',false);
			$('#lane-'+ctr).remove();
		}
		/* End of CPR Section */

		function CheckIfExists()
		{
			var supplier_name = $('#suppliers-name').val();

			$.ajax({
				url: "{{ url('validate-supplier') }}",
				data: { 'name' : supplier_name },
				success:function(response)
				{
					if(response != 'not yet')
					{
						$("#suppliers-name" ).focus();
						$('#error-div').empty().append('<p><b>This Name is already taken.</b></p>').css('color','red').css('display:block !important');
					}
					else
					{
						$('#error-div').empty().append('<p><b>This Name is available.</b></p>').css('color','green');
					}
				}
			});
		}

		function AcceptOnlyNumbers()
		{
			$('.suppliers-mobile-number, .suppliers-phone-number').keypress(function(e) {
			    var a = [];
			    var k = e.which;
			    
			    for (i = 48; i < 58; i++)
			        a.push(i);
			    
			    if (!(a.indexOf(k)>=0))
			        e.preventDefault();
			});
		}

		function ValidateHasFile(type,ctr)
		{
			$('#'+type+'-lane-'+ctr+' .input-group.date').attr('data-provide','datepicker');
			$('#'+type+'-lane-'+ctr+' .input-group.date input').attr('readonly',false);
			var onclick_event = "DisableDatepickerAndAddButton('"+type+"','"+ctr+"')";
			$('#'+type+'-lane-'+ctr+' .fileinput-remove').attr('onclick',onclick_event);

		}

		function DisableDatepickerAndAddButton(type,ctr)
		{
			$('#'+type+'-lane-'+ctr+' .input-group.date').removeAttr('data-provide');
			$('#'+type+'-expiration-'+ctr).val('').attr('readonly',true);
			$('#add-'+type+'-btn-'+ctr).attr('disabled',true);
		}

		function CheckIfEmpty(type,ctr)
		{
			if($('#'+type+'-upload-'+ctr).val() === '')
			{
				$('#notification-modal').modal({backdrop: 'static', keyboard: false});
				var content = "<h4>Select a file first before entering a date.</h4>";
				$('#content-container').append(content);
				$('#notification-modal .modal-footer button').empty().text('Ok');
				$('#'+type+'-expiration-'+ctr).val('').attr('readonly',true);
			} 
			else
			{
				$('#add-'+type+'-btn-'+ctr).attr('disabled',false);
			}
		}

		function EnablePlusMobile(ctr) {
			var mobile = $('#suppliers-mobile-number-'+ctr).val();
			if(mobile.length >= 10) {
				$('#add-mobile-btn-'+ctr).attr('disabled',false);	
			}
		}

		function EnablePlusPhone(ctr) {
			var phone = $('#suppliers-phone-number-'+ctr).val();
			if(phone.length >= 8) {
				$('#add-phone-btn-'+ctr).attr('disabled',false);	
			}
		}

		function AddMobileNumber(ctr) {
			ctr++;
			var mobile_lane = '<div class="col-md-12 mobile-lane-'+ctr+' nopadding" style="margin-top:10px;">\
									<div class="col-md-4 col-md-offset-3 mobile-number-lane-'+ctr+'">\
							            <input id="suppliers-mobile-number-'+ctr+'" type="text" class="form-control suppliers-mobile" name="suppliers-mobile-number[]" placeholder="Enter Supplier Mobile Number '+ctr+'" onkeypress="EnablePlusMobile('+ctr+');" maxlength="11">\
							        </div>\
							        <div class="col-md-2">\
										<button type="button" class="btn btn-success" onclick="AddMobileNumber('+ctr+');" id="add-mobile-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
										<button type="button" class="btn btn-danger" onclick="RemoveMobileNumber('+ctr+');" id="add-mobile-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
									</div>\
								</div>';
			$('#mobile-number-container').append(mobile_lane);
		}

		function RemoveMobileNumber(ctr) {
			var prev = ctr - 1;
			if($('#suppliers-mobile-number-'+prev).val() != '') {
				$('#aadd-mobile-btn-'+prev).attr('disabled',false);
			} else {
				$('#aadd-mobile-btn-'+prev).attr('disabled',true);
			}
			$('.mobile-lane-'+ctr).remove();
		}

		function AddPhoneNumber(ctr) {
			ctr++;
			var phone_lane = '<div class="col-md-12 phone-lane-'+ctr+' nopadding" style="margin-top:10px;">\
									<div class="col-md-4 col-md-offset-3 phone-number-lane-'+ctr+'">\
							            <input id="suppliers-phone-number-'+ctr+'" type="text" class="form-control suppliers-phone" name="suppliers-phone-number[]" placeholder="Enter Supplier Phone Number '+ctr+'" onkeypress="EnablePlusPhone('+ctr+');" maxlength="9">\
							        </div>\
							        <div class="col-md-2">\
										<button type="button" class="btn btn-success" onclick="AddMobileNumber('+ctr+');" id="add-phone-btn-'+ctr+'" disabled><i class="fas fa-plus"></i></button>\
										<button type="button" class="btn btn-danger" onclick="RemoveMobileNumber('+ctr+');" id="add-phone-btn-'+ctr+'"><i class="fas fa-minus"></i></button>\
									</div>\
								</div>';
			$('#phone-number-container').append(phone_lane);
		}

		function RemovePhoneNumber(ctr) {
			var prev = ctr - 1;
			if($('#suppliers-phone-number-'+prev).val() != '') {
				$('#aadd-phone-btn-'+prev).attr('disabled',false);
			} else {
				$('#aadd-phone-btn-'+prev).attr('disabled',true);
			}
			$('.phone-lane-'+ctr).remove();
		}

	</script>

@endsection

@endsection