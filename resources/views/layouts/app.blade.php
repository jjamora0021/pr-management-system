<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Procurement Management System</title>
    <!-- CSS Declarations -->
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('css/fileinput.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('css/explorer-theme.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" type="text/css" >
    <!-- End of CSS Declarations -->

    <!-- Additional CSS -->
        @yield('header')
    <!-- End of Additional CSS -->

    <!-- Favicon Declaration -->
        <link rel="shortcut icon" href="{{ URL::asset('images/logo/antipolo-gov-logo.png') }}" rel="stylesheet" type="image/x-icon" />
    <!-- End of Favicon Declaration -->

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- End of Fonts -->

</head>
<body id="app-layout">
    @yield('content')
    <!-- JS Declarations -->
        <script type="text/javascript" src="{{ URL::asset('js/jquery-2.1.4.min.js') }}"></script>
        @yield('footer')
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap-select.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/fileinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/explorer-theme.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/dataTables.buttons.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/buttons.flash.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/jszip.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/pdfmake.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/vfs_fonts.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/buttons.html5.min.js') }}"></script>
        
        <script type="text/javascript" src="{{ URL::asset('js/snippets/jquery.backstretch.min.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                /*
                    Fullscreen background
                */
                $.backstretch([
                    "images/backgrounds/1.jpg"
                ], {duration: 3000, fade: 750});
            });
        </script>
        @yield('functions')
    <!-- End of JS Declarations -->
</body>
</html>
