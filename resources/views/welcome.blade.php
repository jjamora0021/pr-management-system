@extends('layouts.app')

@section('header')
<!-- CSS -->
    <link href="{{ URL::asset('css/snippets/bootstrap-3.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/snippets/form-elements.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ URL::asset('css/snippets/login-page.css') }}" rel="stylesheet" type="text/css" >
@endsection

@section('content')
<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ url('logout') }}"><i class="fas fa-sign-in-alt"></i> Login</a>
                </li>
                <li>
                    <a href="{{ url('register') }}"><i class="fas fa-user"></i> Register</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-success">
                <div class="panel-heading">Success</div>
                <div class="panel-body" style="color: #3c763d;">
                    A New User Has Been Created.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ URL::asset('js/snippets/jquery.backstretch.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/snippets/login-page.js') }}"></script>
@endsection